"""
Definition of the Chow-Liu tree based algorithm. The class is inherited from the :class:`base.Base`
class.
"""
from operator import itemgetter

import networkx as nx
from cherrytree.utils.base import Base, Node, Star


class ChowLiuTreeAlgorithm(Base):
    """
    It is a special case of the cherry tree algorithm, where k = 2. In this case, the weight of the
    tree depends only on the sum of the bivariate mutual informations, therefore it finds the
    optimal tree structure.
    """
    def __init__(self, data_frame):
        super(ChowLiuTreeAlgorithm, self).__init__(data_frame, 2)

    @staticmethod
    def kruskal_mst_edges(graph):
        """
        Iterate over edges of a Kruskal's algorithm min/max spanning tree.

        :param graph: NetworkX Graph, the graph holding the tree of interest.
        """
        subtrees = nx.utils.UnionFind()
        edges = graph.edges(data=True)
        edges = [(x[2].get('weight', 1) * -1, x[0], x[1], x[2]) for x in edges]
        edges = sorted(edges, key=itemgetter(0, 1, 2))
        for _, start, end, data in edges:
            if subtrees[start] != subtrees[end]:
                yield start, end, data
                subtrees.union(start, end)

    def maximum_spanning_tree(self, graph):
        """
        Returns a maximum spanning tree or forest on an undirected graph *G*.

        :param graph: An undirected graph. If *G* is connected, then the algorithm finds a spanning
            tree. Otherwise, a spanning forest is found.
        :return: A maximum spanning tree.
        """
        edges = self.kruskal_mst_edges(graph)
        edges = list(edges)
        tree = graph.__class__()
        tree.graph.update(graph.graph)
        tree.add_nodes_from(graph.nodes.items())
        tree.add_edges_from(edges)
        return tree

    def build_init_graph(self):
        """
        Build up the base complete graph from the data where the features are the nodes and the edge
        weights are the information content of the two features.

        :return: The weighted complete graph.
        :rtype: :class:`nx.Graph`
        """
        variables = self.input_object.get_variables()
        graph = nx.Graph()
        for comb in self.get_combinations(variables, 2):
            weight = self.input_object.get_inf_cont(*comb)
            graph.add_edge(comb[0], comb[1], weight=weight)
        return graph

    def build_cherry_tree(self):
        """
        Build the Chow-Liu tree by building up a complete graph from the variables as vertices and
        edge weights as the information content between that two variables, and finding the maximum
        spanning tree of this graph.

        :return: The Chow-Liu tree as :class:`CherryTree` object.
        """
        graph = self.build_init_graph()
        graph = self.maximum_spanning_tree(graph)

        for node in graph.nodes():
            clusters = {edge for edge in graph.edges if node in edge}
            if len(clusters) > 1:
                central_sep = Node((node,), 0)
                star = Star(central_sep)
                for cluster in clusters:
                    clust_obj = Node(tuple(sorted(cluster)),
                                     self.input_object.get_inf_cont(*cluster))
                    star.clusters.append(clust_obj)
                self.junction_tree.add_star(star)
        return self.junction_tree
