"""
Definition of the cherry tree based algorithm. The class is inherited from the :class:`base.Base`
class.
"""
from itertools import combinations
from multiprocessing import Pool

import pandas as pd

from cherrytree.utils.base import Base, Node, Star


class CherryTreeAlgorithm(Base):
    """
    This algorithm builds up a given *k* order cherry tree by minimizing the Kullback-Leibler
    divergence in a greedy way. This is equivalent with maximizing the weight of the cherry tree.
    It starts from the 'heaviest' cluster (root cluster) and then consecutively news clusters are
    attached such that the cherry tree condition is fulfilled.
    """
    def __init__(self, input_dataframe: pd.DataFrame, cluster_width: int,
                 use_multiproc: bool = True, **kwargs):
        super(CherryTreeAlgorithm, self).__init__(input_dataframe, cluster_width, **kwargs)
        self.use_multiproc = use_multiproc

    def get_root(self):
        """
        Returns the most heaviest cluster as a :class:`base.Star` object which will be the root of
        the cherry tree.

        :rtype: :class:`base.Star` object
        """
        variables = self.input_object.get_variables()
        cluster_combs = self.get_combinations(variables, self.cluster_width, use_include=False,
                                              extra_include=self.include + self.start_from)
        if self.use_multiproc:
            separator_combs = self.get_combinations(variables, self.cluster_width - 1)
            with Pool(8) as pool:
                cluster_weights = pool.starmap(
                    self.input_object.get_inf_cont, cluster_combs)
                separator_weights = pool.starmap(
                    self.input_object.get_inf_cont, separator_combs)
            for var_tuple, weight in zip(cluster_combs + separator_combs,
                                         cluster_weights + separator_weights):
                self.input_object.information[tuple(sorted(var_tuple))] = weight
        else:
            cluster_weights = [self.input_object.get_inf_cont(*comb)
                               for comb in cluster_combs]
        max_weight = max(cluster_weights)
        max_cluster = Node(tuple(sorted(cluster_combs[cluster_weights.index(max_weight)])),
                           max_weight)
        star = Star(clusters=[max_cluster])
        return star

    def get_next_cluster(self):
        """
        Returns the next cluster which can be joined to the cherry tree such that the cherry tree
        condition is fulfilled. The function finds the neighbor cluster and the separator, too.

        :return: The (new cluster, neighbor cluster, separator) tuple.
        :rtype: tuple
        """
        variables = self.input_object.get_variables()
        variables_not_in_tree = variables - self.junction_tree.variables
        possible_mutual_infs = [(self.input_object.get_inf_cont(*(sep + (x,))) -
                                 self.input_object.get_inf_cont(*sep),
                                 tuple(sorted(sep + (x,))),
                                 cluster, sep)
                                for x in variables_not_in_tree
                                for cluster in self.junction_tree.clusters
                                for sep in combinations(cluster, self.cluster_width - 1)
                                if set(self.include).issubset(set(sep + (x,)))
                                and (not set(self.exclude).issubset(sep) or not self.exclude)]
        sorted_infs = sorted(possible_mutual_infs, key=lambda tup: tup[0], reverse=True)
        cluster_obj = Node(tuple(sorted(sorted_infs[0][1])),
                           self.input_object.get_inf_cont(*sorted_infs[0][1]))
        cluster_in = sorted_infs[0][2]
        separator = Node(sorted_infs[0][3],
                         self.input_object.get_inf_cont(*(sorted_infs[0][3])))
        return cluster_obj, cluster_in, separator

    def build_cherry_tree(self):
        """
        Handling the main logic of building up the cherry tree.

        :return: The built cherry tree.
        :rtype: :class:`base.CherryTree`
        """
        root = self.get_root()
        self.junction_tree.add_star(root)
        variables = self.input_object.get_variables()
        while self.junction_tree.variables != variables:
            cluster, cluster_in, separator = self.get_next_cluster()
            if separator.name in [sep.central_sep.name for sep in self.junction_tree.stars
                                  if sep.central_sep]:
                sep_star = self.junction_tree.get_star_by_central(separator.name)
                self.junction_tree.update_star_with(sep_star, cluster)
            else:
                if not self.junction_tree.stars[0].central_sep:
                    self.junction_tree.stars[0].central_sep = separator
                    self.junction_tree.update_star_with(self.junction_tree.stars[0], cluster)
                else:
                    cluster_in = Node(cluster_in, self.input_object.get_inf_cont(*cluster_in))
                    new_star = Star(separator, clusters=[cluster, cluster_in])
                    self.junction_tree.add_star(new_star)
        return self.junction_tree
