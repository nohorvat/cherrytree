"""
Definition of the sequentially improved cherry tree based algorithm. The class is inherited from
the :class:`base.Base` class. The function we have to define is the *get_root* function.
"""
from multiprocessing import Pool

from cherrytree.algorithms.chow_liu_tree import ChowLiuTreeAlgorithm
from cherrytree.algorithms.cherry import CherryTreeAlgorithm
from cherrytree.utils.base import Base, CherryTree


class SequentialCherryTreeAlgorithm(Base):
    """
    The input of the sequentially improved cherry tree algorithm is a k-th order cherry tree. This
    will be improved to a given l order cherry tree (where l > k). If the *base_tree* is set to None
    (default), then the l order cherry tree is constructed from the Chow-Liu tree (second order
    cherry tree).
    """
    def __init__(self, data_frame, cluster_width, base_tree=None):
        self.data_frame = data_frame
        self.base_tree = base_tree
        if base_tree:
            self._check_base_tree()
        super(SequentialCherryTreeAlgorithm, self).__init__(data_frame, cluster_width)
        self._check_width()
        self.junction_tree = CherryTree()

    def _check_base_tree(self):
        """
        Check whether the *base_tree* parameter is a CherryTree object or None.

        :return: None
        """
        if not isinstance(self.base_tree, CherryTree):
            raise ValueError('The base tree must be a CherryTree object.')

    def _check_width(self):
        """
        Check whether the *base_tree* and the *cluster_width* parameter are correct together. The
        cluster width must be strictly grater than the actual width of the base tree. If there is no
        base tree, the actual width is set to 2.

        :return: None or raises ValueError.
        """
        actual_width = self.base_tree.get_width() if self.base_tree else 2
        if actual_width >= self.cluster_width:
            raise ValueError('The base junction tree is wider or has the same width as the given '
                             'cluster width.')

    def get_big_star(self, star, actual_width):
        """
        Increase the *k*-clusters in *star* into *k+1*-clusters by building up a new cherry tree
        from the variables in *star* with the constraint that the central separator of *star* is in
        all clusters.

        :param star: The star of *k*-width clusters to increase into *k+1*-width clusters.
        :param actual_width: The width of the increased clusters.

        :return: The (*star*, list of the increased star(s)) pair.
        :rtype: tuple
        """
        nodes = list({feat for clust in star.clusters for feat in clust.name})
        return (star,
                CherryTreeAlgorithm(self.input_object.dataframe[nodes].copy(), actual_width,
                                    include=star.central_sep.name,
                                    use_multiproc=False).build_cherry_tree().stars)

    def build_cherry_tree(self):
        """
        Handling the main logic of building up a cherry tree.

        The steps:
            * Select the root cluster.
            * While all variables do not occur in the cherry tree, select the next cluster.

        :return: The built :class:`CherryTree` object.
        """
        base_tree = self.base_tree or ChowLiuTreeAlgorithm(self.data_frame).build_cherry_tree()
        actual_width = base_tree.get_width()
        sequential_cherry = None

        while actual_width != self.cluster_width:
            actual_width += 1
            sequential_cherry = CherryTree()
            stars_in = []

            with Pool(8) as pool:
                big_stars = pool.starmap(self.get_big_star,
                                         [(star, actual_width) for star in base_tree.stars])
            while big_stars:
                for star_tup in big_stars:
                    star, big_star = star_tup
                    nbr_clust, nbr_star = None, None
                    for star_in in stars_in:
                        insec = star_in.intersect(star)
                        if insec:
                            nbr_star = star_in
                            nbr_clust = insec
                    if nbr_star or not stars_in:
                        stars_in.append(star)
                        for base_star in big_star:
                            sequential_cherry.add_star(base_star, sep=nbr_clust)
                        del big_stars[big_stars.index(star_tup)]

            base_tree = sequential_cherry

        self.junction_tree = sequential_cherry
        return sequential_cherry
