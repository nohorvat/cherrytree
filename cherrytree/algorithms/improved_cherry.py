"""
Definition of the improved cherry tree based algorithm. The class is inherited
from the :class:`base.Base` class. The function we have to define is the *get_root* function.
"""
from multiprocessing import Pool

from cherrytree.algorithms.cherry import CherryTreeAlgorithm
from cherrytree.utils.base import Node, Star


class ImprovedCherryTreeAlgorithm(CherryTreeAlgorithm):
    """
    The improved cherry tree algorithm differs from the cherry tree algorithm in the starting step.
    The improved algorithm starts from two clusters with the highest weight (instead of one cluster
    as in the cherry tree algorithm).
    """
    def _calculate_bud_weight(self, cluster_1, cluster_2, separator):
        """
        Calculate the weights of *cluster_1*, *separator* and *cluster_2*.

        :param cluster_1: The first cluster to calculate.
        :param cluster_2: The second cluster to calculate.
        :param separator: The separator between *cluster_1* and *cluster_2* to calculate.
        :return: The calculated weights as a tuple.
        """
        return (self.input_object.get_inf_cont(*cluster_1),
                self.input_object.get_inf_cont(*cluster_2),
                self.input_object.get_inf_cont(*separator))

    def get_root(self):
        """
        Return the root(s) of the cherry tree as a :class:`CherryTree` object.
        The two clusters will be chosen which are maximizing the following expression:

        .. math::

            I(x_1, x_2, x_3) - I(x_1, x_2) + I(x_1, x_2, x_4),

        where :math:`I(x_1, x_2, x_3)` and :math:`I(x_1, x_2, x_4)` are the information content of
        the clusters and :math:`I(x_1, x_2)` is the information content of the separator.

        :return: The root component of the cherry tree as a sub cherry tree.
        :rtype: :class:`Star`
        """
        variables = self.input_object.get_variables()

        cluster_combs = self.get_combinations(
            variables, self.cluster_width) + \
            self.get_combinations(variables, self.cluster_width - 1)
        with Pool(8) as pool:
            cluster_weights = pool.starmap(
                self.input_object.get_inf_cont, list(cluster_combs))
        self.input_object.information = {
            tuple(sorted(var_tuple)): weight
            for var_tuple, weight in zip(cluster_combs, cluster_weights)}

        possible_intersecs = self.get_combinations(variables, self.cluster_width - 1)
        pair_list = [(poss_sep + (var[0],), poss_sep + (var[1],), poss_sep)
                     for poss_sep in possible_intersecs
                     for var in self.get_combinations(variables - set(poss_sep), 2,
                                                      use_include=False)
                     if set(self.start_from).issubset(set(poss_sep + var))]
        cluster_weights = [self._calculate_bud_weight(*pair) for pair in pair_list]
        max_weight = max(cluster_weights, key=lambda tup: tup[0] + tup[1] - tup[2])
        max_pair = pair_list[cluster_weights.index(max_weight)]
        cluster_1 = Node(tuple(sorted(max_pair[0])), max_weight[0])
        cluster_2 = Node(tuple(sorted(max_pair[1])), max_weight[1])
        separator = Node(tuple(sorted(max_pair[2])), max_weight[2])
        star = Star(separator, clusters=[cluster_1, cluster_2])
        return star
