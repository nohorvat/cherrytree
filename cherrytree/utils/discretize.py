"""
Discretize module which provides several discretization methods. The discretization is needed
in order to the algorithms can be used.
"""
from typing import Iterable, Union

import numpy as np
import pandas as pd


def discretize_quantile(dataframe: pd.DataFrame, columns: Iterable[str],
                        quantiles: Union[int, Iterable[float]]):
    """
    Quantile-based discretization function using the pandas built-in :py:class:`pandas.qcut`
    function. Discretize variable into equal-sized buckets based on rank or based on sample
    quantiles.

    :Example:

    >>> import pandas as pd
    >>> from cherrytree.utils.discretize import discretize_quantile
    >>> dataframe = pd.DataFrame(data=[{'A': 5.12, 'B': 6.56, 'C': 7.98},
    ...                                {'A': -0.45, 'B': 4.2, 'C': 1.2},
    ...                                {'A': 3.1, 'B': -1.02, 'C': 11}])
    >>> discretize_quantile(dataframe, ['A', 'C'], 3)
    >>> dataframe #doctest: +NORMALIZE_WHITESPACE
        A       B   C
    0   2    6.56   1
    1   0    4.20   0
    2   1   -1.02   2
    >>> discretize_quantile(dataframe, ['B'], [0, .25, .75, 1])
    >>> dataframe #doctest: +NORMALIZE_WHITESPACE
        A    B   C
    0   2    2   1
    1   0    1   0
    2   1    0   2

    :param dataframe: The dataframe which have to be discretized.
    :param columns: The name of the columns which are selected to discretize with this method.
    :param quantiles: Number of quantiles. 10 for deciles, 4 for quartiles, etc. Alternately
        array of quantiles, e.g. [0, .25, .5, .75, 1.] for quartiles.
    :return: None
    """
    for column in columns:
        dataframe[column] = pd.qcut(dataframe[column], quantiles, labels=False)


def discretize_digitize(dataframe: pd.DataFrame, columns: Iterable[str], bins: Iterable[float]):
    """
    Set the indices of the bins to which each value in input array belongs using
    the numpy built-in function :py:class:`digitize`.
    Each index ``i`` returned is such that ``bins[i-1] <= x < bins[i]`` if
    `bins` is monotonically increasing, or ``bins[i-1] > x >= bins[i]`` if
    `bins` is monotonically decreasing. If values in proper column are beyond the
    bounds of *bins*, 0 or *len(bins)* is returned as appropriate. The right
    bin is opened so that the index ``i`` is such
    that ``bins[i-1] <= x < bins[i]`` or ``bins[i-1] > x >= bins[i]`` if `bins`
    is monotonically increasing or decreasing, respectively.

    :Example:

    >>> import pandas as pd
    >>> from cherrytree.utils.discretize import discretize_digitize
    >>> dataframe = pd.DataFrame(data=[{'A': 5.12, 'B': 6.56, 'C': 7.98},
    ...                                {'A': -0.45, 'B': 4.2, 'C': 1.2},
    ...                                {'A': 3.1, 'B': -1.02, 'C': 11}])
    >>> discretize_digitize(dataframe, ['A', 'C'], [2.1, 5.1, 12.5])
    >>> dataframe #doctest: +NORMALIZE_WHITESPACE
        A       B   C
    0   2    6.56   2
    1   0    4.20   0
    2   1   -1.02   2

    :param dataframe: The dataframe which have to be discretized.
    :param columns:  The name of the columns which are selected to discretize with this method.
    :param bins: Array of bins. It has to be 1-dimensional and monotonic.
    :return: None
    """
    for column in columns:
        dataframe[column] = np.digitize(dataframe[column], bins, right=False)
