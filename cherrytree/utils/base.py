"""
Base module for managing the input :py:class:`pandas.DataFrame` object, validate the input data
frame, defining :class:`Cluster` and :class:`Separator` classes, visualising the cherry tree and
managing the logic to build up the cherry tree in the :class:`Base` class.
"""
import functools
import math
import operator
import uuid
from collections import namedtuple
from itertools import combinations
from typing import Iterable

import numpy as np
import pandas as pd
from graphviz import Source


Node = namedtuple('Node', 'name weight')


class InputHandler:
    """
    Class for calculating input related properties, such as probabilities and information content.
    Initially adds a probability column to the **dataframe** using random generated name to
    avoid header name duplication.

    .. note:: The given dataframe should contain only the variables which are needed,
        the column headers should be the names of the variables. In the results,
        the variables will be referred as this name.

    :param dataframe: The data we want to work on as a :py:class:`pandas.DataFrame` object.
    """
    def __init__(self, dataframe: pd.DataFrame):
        self.dataframe = self.check_dataframe(dataframe.copy())
        self.number_of_realisations = self.dataframe.shape[0]
        self.probabilities = {}
        self.prod_probabilities = {}
        self.information = {}
        self.freq_column_name = 'freq-{}'.format(uuid.uuid4().hex[:8])
        self.add_freq_column()

    @staticmethod
    def check_dataframe(dataframe):
        """
        Check some basic properties of the dataframe that the algorithms can be used on it, e.g.
        it must be filled out fully.

        :param dataframe: The data we want to work on as a :py:class:`pandas.DataFrame` object.
        :return: The input dataframe.
        :rtype: :py:class:`pandas.DataFrame`
        """
        if dataframe.isnull().any().any():
            raise ValueError('The algorithms can not work with missing data. Please drop or impute '
                             'the leaky rows.')
        return dataframe

    def add_freq_column(self):
        """
        Initially set the frequency column at *dataframe* to 1. It will count the occurrences of
        each of the sample rows.

        :return: None
        """
        self.dataframe[self.freq_column_name] = 1

    def get_variables(self):
        """
        Returns the name of the variables in a set (without the probability column).

        :return: The name of the variables as strings in a set.
        :rtype: set
        """
        vars_set = set(self.dataframe.columns)
        vars_set.remove(self.freq_column_name)
        return vars_set

    def get_frequency(self, *variables: str):
        """
        Given a subset of random **variables**, a list of all its realizations is given and to each
        realization the number of its occurrence is associated in the format of
        {(var_name_1, var_name_2, ...): #occurences, ...}.

        :Example:

        >>> import pandas as pd
        >>> from cherrytree.utils.base import InputHandler
        >>> dataframe = pd.DataFrame(data=[{'A': 5, 'B': 6, 'C': 7}, {'A': 5, 'B': 4, 'C': 7}])
        >>> a = InputHandler(dataframe)
        >>> a.get_frequency('A', 'B', 'C')
        {(5, 6, 7): 1, (5, 4, 7): 1}
        >>> a.get_frequency('A', 'C')
        {(5, 7): 2}
        >>> a.get_frequency('B')
        {(6,): 1, (4,): 1}

        :param variables: The name of the variables for which we get the dictionary of the possible
        realizations and number of occurences.
        :return: The dictionary of the possible realizations and number of occurences.
        :rtype: dict
        """
        variable_tuple = tuple(sorted(variables))
        sol = self.probabilities.get(variable_tuple)
        if sol:
            return sol
        ser_with_tup_labs = self.dataframe.groupby(list(variable_tuple))[
            self.freq_column_name].count()
        self.probabilities[variable_tuple] = ser_with_tup_labs.to_dict()
        return self.probabilities[variable_tuple]

    def get_prod_probability(self, variable_tuple: tuple):
        """
        Given a subset of random **variables**, a list of all its realizations is given and to each
        realization the number of its approx. (by independence) occurrence is associated in the
        format of {(var_name_1, var_name_2, ...): #occurences, ...}.

        """
        sol = self.prod_probabilities.get(variable_tuple)
        if sol:
            return sol
        ser_with_tup_ind = self.get_frequency(*variable_tuple)
        tup_indices = ser_with_tup_ind.keys()

        def product_prob(indices):
            return functools.reduce(operator.mul,
                                    [self.get_frequency(var)[idx]
                                     for var, idx in zip(variable_tuple, indices)], 1)

        prod_pob_hash = dict(zip(tup_indices, [product_prob(x) for x in tup_indices]))
        self.prod_probabilities[variable_tuple] = prod_pob_hash
        return self.prod_probabilities[variable_tuple]

    def get_inf_cont(self, *variables: str):
        """
        Calculate the information content of **variables** based on the sample
        and return it as float.

        The information content is a generalization of the mutual information, and is calculated
        with the following formula:

        .. math::

            I(x_1, \\dots, x_n) = \\sum_{r} P^r(x_1, \\dots, x_n) \\mathrm{log}_2 \\
            \\frac{P^r(x_1, \\dots, x_n)}{P^r(x_1) \\dots P^r(x_n)},

        where :math:`P^r(.)` is the probability for the realisation *r*.

        :Example:

        >>> import pandas as pd
        >>> from cherrytree.utils.base import InputHandler
        >>> dataframe = pd.DataFrame(data=[{'A': 1, 'B': 6, 'C': 7}, {'A': 5, 'B': 4, 'C': 7}])
        >>> a = InputHandler(dataframe)
        >>> a.get_frequency('A', 'B')
        {(5, 4): 1, (1, 6): 1}
        >>> a.get_frequency('A')
        {(5,): 1, (1,): 1}
        >>> a.get_frequency('B')
        {(6,): 1, (4,): 1}
        >>> a.get_inf_cont('A', 'B')
        1.0

        :param variables: The name of the variables for which we calculate the information content.
        :return: The information content of **variables**.
        :rtype: float
        """
        variable_tuple = tuple(sorted(variables))
        sol = self.information.get(variable_tuple)
        if sol:
            return sol
        if len(variable_tuple) == 1:
            self.information[variable_tuple] = 0
            return self.information[variable_tuple]
        good_weights = np.fromiter(self.get_frequency(*variable_tuple).values(), dtype=float)
        good_weights = good_weights / self.number_of_realisations
        prod_weights = np.fromiter(self.get_prod_probability(variable_tuple).values(), dtype=float)
        for _ in variable_tuple:
            prod_weights = prod_weights / self.number_of_realisations
        for_log = np.divide(good_weights, prod_weights)
        summand = np.dot(good_weights, np.log2(for_log))
        self.information[variable_tuple] = summand
        return self.information[variable_tuple]


class Star:
    """
    Class to describe the star object. One star is consists of a central separator (:class:`Node`
    object) and a list of clusters (:class:`Node` objects). The intersection of every two clusters
    is the central separator.

    The parameters are the following:
        * **central_sep**: The central separator as a :class:`Node` object
        * **clusters**: The list of the clusters as :class:`Node` objects
    """
    def __init__(self, central_sep: Node = None, clusters: Iterable[Node] = None):
        self.central_sep = central_sep
        self.clusters = clusters or []

    def intersect(self, other):
        """
        Checks if two star has common cluster. If they intersect the function returns the common
        cluster, else None.

        :param other: The other star we check whether it intersects as a :class:`Star` object.
        :return: The intersection of the two stars if they intersect else None.
        :rtype: :class:`Node` or NoneType
        """
        for cluster in self.clusters:
            for cluster_in in other.clusters:
                if cluster.name == cluster_in.name:
                    return cluster
        return None

    def __repr__(self):
        central_name = self.central_sep.name if self.central_sep else None
        return str(central_name) + '**' + str([clust.name for clust in self.clusters])

    def __eq__(self, other):
        clust_same = \
            {clust.name for clust in self.clusters} == {clust.name for clust in other.clusters}
        if self.central_sep and other.central_sep:
            return self.central_sep.name == other.central_sep.name and clust_same
        if self.central_sep or other.central_sep:
            return self.central_sep is None and other.central_sep is None and clust_same
        return clust_same


class JunctionTree:
    """
    Class for managing the junction tree related commands. The junction tree object consists
    clusters and separators as :class:`Cluster` and :class:`Separator` objects. The union of the
    clusters contains all the random variables. The variables in the clusters have to satisfy the
    so called running intersection property. Each cluster contains a set of maximum size *k*, and
    each separator contains a set of maximum size *k-1*. The weight of the junction tree is given
    by the following formula. As larger this weight is, as better the approximation given by the
    junction tree is.

    .. math::

        W = \\sum_{\\mathrm{cluster}} I(\\mathrm{cluster}) -
        \\sum_{\\mathrm{separator}} I(\\mathrm{separator})
    """
    def __init__(self):
        self.clusters = []
        self.stars = []
        self.variables = set()
        self.weight = 0

    def get_cluster_sep_hash(self):
        """
        Transform the star-based cherry tree structure into cluster-based structure.

        :return: The cluster-based structure as a {cluster: separator, ...} dictionary.
        """
        clust_sep_hash = {self.stars[0].clusters[0].name: None}
        while len(clust_sep_hash) != len(self.clusters):
            for star in self.stars:
                if any([clust.name in clust_sep_hash for clust in star.clusters]):
                    for cluster in star.clusters:
                        if cluster.name not in clust_sep_hash:
                            clust_sep_hash[cluster.name] = star.central_sep.name
        return clust_sep_hash

    def check_running_intersection(self):
        """
        Checks if the running intersection property (RIP) holds: If :math:`X_i` and :math:`X_j`
        both contain a vertex *v*, then all nodes :math:`X_k` of the tree in the (unique) path
        between :math:`X_i` and :math:`X_j` contain *v* as well.

        :return: None if the RIP holds else raises ValueError.
        """
        clust_sep_hash = self.get_cluster_sep_hash()
        for var in self.variables:
            roots = set()
            for cluster, sep in clust_sep_hash.items():
                sep = sep if sep else tuple()
                if var in cluster and var not in sep:
                    roots.add(cluster)
            if len(roots) > 1:
                raise ValueError('The running intersection property does not hold.')

    def get_width(self):
        """
        Gets the number of the variables in the largest cluster which is called the width of the
        junction tree. If there are no clusters the width is 0.

        :rtype: int
        """
        if self.clusters:
            return max([len(x) for x in self.clusters])
        return 0

    def update_star_with(self, star, cluster):
        """
        Adds *cluster* to the *star* which is in the junction tree. Addition steps:
            * Adds *cluster* to the *star*.clusters list.
            * Adds *cluster*.name to the tree clusters list.
            * Updates the tree variables.
            * Adds and subtracts the weight of the *cluster* and the *star*.central_sep.

        :param star: The star to update as a :class:`Star` object.
        :param cluster: The cluster to add as a :class:`Node` object.
        :return: None
        """
        if cluster.name not in [clust.name for clust in star.clusters]:
            star.clusters.append(cluster)
            if cluster.name not in self.clusters:
                self.clusters.append(cluster.name)
                self.weight += cluster.weight
                self.variables.update(cluster.name)
            self.weight -= star.central_sep.weight

    def remove_from_star(self, star, cluster):
        """
        Removes *cluster* from the *star* which is in the junction tree. Deletion steps:
            * Removes *cluster* from the *star*.clusters list.
            * Removes *cluster*.name from the tree clusters list.
            * Updates the tree variables.
            * Subtracts and adds the weight of the *cluster* and the *star*.central_sep.

        :param star: The star to update as a :class:`Star` object.
        :param cluster: The cluster to remove as a :class:`Node` object.
        :return: None
        """
        if cluster.name in [clust.name for clust in star.clusters]:
            for index, cluster_in in enumerate(star.clusters):
                if cluster_in.name == cluster.name:
                    del star.clusters[index]
                    break
            if cluster.name in self.clusters:
                del self.clusters[self.clusters.index(cluster.name)]
            self.weight -= cluster.weight
            self.weight += star.central_sep.weight
            self.variables = {feat for clust in star.clusters
                              for feat in clust.name if clust != cluster}

    def get_star_by_central(self, central_sep):
        """
        Returns the star which central separator has a name *central_sep*.

        :param central_sep: The name of the central separator of the star we wish to find as a
            tuple.
        :return: The star which central separator is *central_sep*. If there is no star with the
            given central separator, it raises ValueError.
        :rtype: :class:`Star` object or ValueError
        """
        for star in self.stars:
            if star.central_sep and star.central_sep.name == central_sep:
                return star
        raise ValueError('There is no star with the following central '
                         'separator: {}'.format(central_sep))

    def _get_neighbor_star_cluster(self, separator: Node):
        """
        Finds the star which has a cluster which contains *separator* and returns this proper
        (star, cluster) tuple. If there is no such star, the function returns None.

        :param separator: The name of the separator we want to find in a cluster as a subset.
        :return: The proper (star, cluster) pair which will be the neighbor of the new star.
        :rtype: tuple or NoneType
        """
        for star in self.stars:
            for cluster in star.clusters:
                if set(separator.name) < set(cluster.name):
                    return star, cluster
        return None

    def extend_with_star(self, star):
        """
        Adds *star* to the stars list of the tree. Addition steps:
            * Appends *star* in the stars list.
            * Updates variables set and clusters list with the clusters in *star* if needed.
            * Updates the weights with adding the weight of the clusters and subtracting the weight
                of the separators.

        :param star: The star to join to the tree.
        :return: None
        """
        if star not in self.stars:
            self.stars.append(star)
            for cluster in star.clusters:
                self.variables.update(cluster.name)
                if cluster.name not in self.clusters:
                    self.clusters.append(cluster.name)
                    self.weight += cluster.weight
            if star.central_sep:
                self.weight -= star.central_sep.weight * (len(star.clusters) - 1)

    def add_star(self, star: Star, sep=None):
        """
        Handling the main logic of adding a new star to the tree. There are different cases based
        on if there is central separator or if the *sep* parameter is filled.

        :param star: The star to join to the tree.
        :param sep: The separator with which the *star* will join to the tree.
        """
        if not self.stars or not sep:
            self.extend_with_star(star)

        else:
            nbr_star, nbr_clust = self._get_neighbor_star_cluster(sep)
            if not nbr_star.central_sep:
                nbr_star.central_sep = sep
                if not star.central_sep:
                    self.update_star_with(nbr_star, star.clusters[0])

                elif star.central_sep.name == sep.name:
                    for cluster in star.clusters:
                        self.update_star_with(nbr_star, cluster)

                else:
                    for cluster in star.clusters:
                        if set(sep.name) < set(cluster.name):
                            self.update_star_with(nbr_star, cluster)
                            break
                    self.extend_with_star(star)

            else:
                if not star.central_sep:
                    if sep.name == nbr_star.central_sep.name:
                        self.update_star_with(nbr_star, star.clusters[0])

                    else:
                        star.clusters.append(nbr_clust)
                        star.central_sep = sep
                        self.extend_with_star(star)

                elif sep.name == nbr_star.central_sep.name:
                    if sep.name == star.central_sep.name:
                        for cluster in star.clusters:
                            self.update_star_with(nbr_star, cluster)

                    else:
                        for cluster in star.clusters:
                            if set(sep.name) < set(cluster.name):
                                self.update_star_with(nbr_star, cluster)
                                break
                        self.extend_with_star(star)

                else:
                    if star.central_sep.name == nbr_star.central_sep.name:
                        raise ValueError('A kivételes eset megtörtént.')

                    connect_star = Star(sep)
                    connect_star.clusters.append(nbr_clust)
                    for cluster in star.clusters:
                        if set(sep.name) < set(cluster.name):
                            connect_star.clusters.append(cluster)
                            break
                    self.extend_with_star(connect_star)
                    self.extend_with_star(star)

    def get_relevant_variables(self, target_variable: str):
        """
        Returns the relevant variables for a given **target_variable**. The relevant variables are
        those which share the same clusters with the **target_variable**. The relevance means
        neighbors in a Markov network with the local Markov property.

        :param target_variable: The name of the variable to examine.
        :return: The set of the relevant variables.
        :rtype: set
        """
        relevant_variables = set()
        for cluster in self.clusters:
            if target_variable in cluster:
                relevant_variables.update(cluster)
        relevant_variables.remove(target_variable)
        return relevant_variables

    @staticmethod
    def format_str(string):
        """
        Formatting the given *string* in order to draw out the cherry tree. Remove the set and
        tuple-related brackets.

        :param string: The string to format.
        :return: The formatted string.
        """
        return str(string).replace("'", "").replace('(', '').replace(')', '') \
            .replace('{', '').replace('}', '')

    def draw_markov_graph(self, view=True):  # pragma: no cover
        """
        The cherry tree can be represented also in the form of a triangulated graph. The variables
        belonging to a cluster are connected to each other. To each cherry tree a Markov network
        enhanced with the local Markov property, represented by a triangular graph can be
        associated.
        Draws the Markov network using the package :py:class:`networkx` and :py:class:`matplotlib`.

        :return: None
        """
        edge_list = []
        for cluster in self.clusters:
            for comb in combinations(cluster, 2):
                edge_list.append((comb[0], comb[1]))

        dot_text = """graph G {
                node [shape=box, style=rounded, fontsize="18"];
                edge [penwidth=0.1, arrowhead=none];
                graph [splines=line, pad="0.2", nodesep="0.5"];
                """

        index = 0
        index_hash = {}

        for edge in edge_list:
            for node in edge:
                if node not in index_hash:
                    dot_text += '\n{} [label="{}"];\n'.format(index, self.format_str(node))
                    index_hash[node] = index
                    index += 1
            if '{} -- {};'.format(index_hash.get(edge[0]), index_hash.get(edge[1])) not in dot_text:
                dot_text += '{} -- {};'.format(index_hash.get(edge[0]), index_hash.get(edge[1]))

        dot_text += "}"

        src = Source(dot_text)
        src.render('markov_graph', view=view, cleanup=True)

    def draw_cherry_tree(self, view=True):  # pragma: no cover
        """
        For the visualization of the obtained cherry tree structure the clusters and the separators
        are represented in tree form. The clusters correspond to the nodes of the tree,
        the separators correspond to the edges of the tree.
        Draws the cherry tree using the package :py:class:`graphviz` and :py:class:`matplotlib`.

        :return: None
        """
        if len(self.clusters) == 1:
            edge_list = [(self.clusters[0],)]
        else:
            edge_list = []
            for star in self.stars:
                for index in range(len(star.clusters) - 1):
                    edge_list.append((star.clusters[index].name, star.clusters[index + 1].name))

        dot_text = """digraph G {
        node [shape=box, style=rounded, fontsize="18"];
        edge [constrain=true, arrowhead=none];
        graph [pad="0.5", nodesep="1", ranksep="0.3", rankdir="TB"];
        """

        index = 0
        clust_hash = {}
        sep_hash = {}

        if len(edge_list) == 1 and len(edge_list[0]) == 1:
            dot_text += '\n{} [label="{}"];\n'.format(index, self.format_str(edge_list[0][0]))
        else:
            while edge_list:
                for edge in edge_list:
                    if edge[0] in clust_hash or edge[1] in clust_hash or not clust_hash:
                        for node in [node_ni for node_ni in edge if node_ni not in clust_hash]:
                            dot_text += '\n{} [label="{}"];\n'.format(index,
                                                                      self.format_str(node))
                            clust_hash[node] = index
                            index += 1
                        start, end = edge  # pylint: disable=unbalanced-tuple-unpacking
                        sep = tuple(sorted(set(start).intersection(set(end))))
                        dot_text += \
                            '\n{} [label="{}", style="filled, rounded, dashed", color=red, ' \
                            'fillcolor=white, fontsize="13"];\n'.format(index, self.format_str(sep))
                        sep_hash[sep] = index
                        index += 1

                        dot_text += '{} -> {};'.format(clust_hash.get(start), sep_hash.get(sep))
                        dot_text += '{} -> {};'.format(sep_hash.get(sep), clust_hash.get(end))
                        del edge_list[edge_list.index(edge)]

        dot_text += "}"

        src = Source(dot_text)
        src.render('cherry_tree', view=view, cleanup=True)

    def __repr__(self):
        return str({'stars': self.stars, 'weight': self.weight})

    def __eq__(self, other):
        stars_equal = all([star in other.stars for star in self.stars]) and \
                      all([star in self.stars for star in other.stars])
        return set(self.clusters) == set(other.clusters) and stars_equal \
            and self.variables == other.variables \
            and round(self.weight, 8) == round(other.weight, 8)


class CherryTree(JunctionTree):
    """
    Class for managing the cherry tree related commands. The cherry tree object consists
    clusters and separators as :class:`Cluster` and :class:`Separator` objects. The union of the
    clusters contains all the random variables. The variables in the clusters have to satisfy the
    so called running intersection property. Each cluster contains a set of size *k*, and each
    separator contains a set of size *k-1*. The weight of the cherry tree is given by the following
    formula. As larger this weight is, as better the approximation given by the cherry tree is.

    .. math::

        W = \\sum_{\\mathrm{cluster}} I(\\mathrm{cluster}) -
        \\sum_{\\mathrm{separator}} I(\\mathrm{separator})
    """
    def get_width(self):
        """
        Gets the number of the variables in any cluster which is called the width of the cherry
        tree. If there are no clusters the width is 0.

        :rtype: int
        """
        if self.clusters:
            return len(self.clusters[0])
        return 0


class Base:
    """
    Abstract class for build up the cherry tree. The following function
    have to be implemented in the child classes: *get_root*.

    :param input_dataframe: The data we want to work on as a :py:class:`pandas.DataFrame` object.
    :param cluster_width: The width of the clusters (how many variables are in a cluster)
        in the cherry tree. The width of the separators will be *cluster_width* - 1.
    :param include: The name of the variables which must occur in all clusters
        (and hence separators). By default it is None. so we consider all possible combinations
        of the variables.
    :param exclude: The name of the variables which must occur in only one cluster
        (and hence none of the separators). By default it is None. so we consider all possible
        combinations of the variables.
    :param start_from: The name of the variables which must occur in the root cluster(s). By
        default it is None. so we consider all possible combinations of the variables.
    """
    def __init__(self, input_dataframe: pd.DataFrame, cluster_width: int, **constraint_kwargs):
        self.input_object = InputHandler(input_dataframe)
        self.cluster_width = self._check_cluster_width(cluster_width)
        self.exclude = self._check_exclude(constraint_kwargs.get('exclude'))
        self.start_from = self._check_start_from(constraint_kwargs.get('start_from'))
        self.include = self._check_include(constraint_kwargs.get('include'))
        self.junction_tree = CherryTree()

    def _check_cluster_width(self, cluster_width):
        """
        Check whether the *cluster_width* parameter is correct. The cluster width parameter must be
        an integer between 1 and the number of variables in the data.

        :param cluster_width: The cluster width to check.
        :return: The cluster width or raises ValueError.
        """
        if isinstance(cluster_width, int) \
                and 1 < cluster_width <= len(self.input_object.get_variables()):
            return cluster_width
        raise ValueError('The cluster_width parameter is not valid. Please check the requirements '
                         'in the documentation.')

    def _check_include(self, include):
        """
        Check whether the *include* parameter is correct. The length of the include parameter must
        be less than the cluster width. The include and exclude parameters can not contain common
        variables.

        :param include: The value of the include parameter to check.
        :return: The include parameter as a tuple.
        """
        if include:
            if set(include).intersection(set(self.exclude)):
                raise ValueError('The include and exclude parameters can not contain common '
                                 'variables.')
            if len(set(self.start_from + include)) > self.cluster_width:
                raise ValueError('The length of the parameters include and start_from together '
                                 'must be less than or equal to the cluster_width.')
            if len(include) >= self.cluster_width:
                raise ValueError('The length of the parameter include must be less than the '
                                 'cluster_width.')
            return include
        return tuple()

    def _check_exclude(self, exclude):
        """
        Check whether the *exclude* parameter is correct. The length of the exclude parameter must
        be less than the number of variables - (cluster width - 1).

        :param exclude: The value of the exclude parameter to check.
        :return: The exclude parameter as a tuple.
        """
        if exclude:
            if len(exclude) >= len(self.input_object.get_variables()) - \
                    (self.cluster_width - 1):
                raise ValueError('The length of the parameter exclude must be less than '
                                 'the #{variables} - (cluster_width - 1).')
            return exclude
        return tuple()

    def _check_start_from(self, start_from):
        """
        Check whether the *start_from* parameter is correct. The length of the start_from parameter
        must be less than or equal to the cluster width.

        :param start_from: The value of the start_from parameter to check.
        :return: The start_from parameter as a tuple.
        """
        if start_from:
            if len(start_from) > self.cluster_width:
                raise ValueError('The length of the parameter start_from must be less than or '
                                 'equal to the cluster_width.')
            return start_from
        return tuple()

    def get_combinations(self, variables_list: Iterable[str], width: int, use_include: bool = True,
                         extra_include=tuple()):
        """
        Return the **width**-combinations (without replacement) of the variables in
        **variables_list**. If the *include* parameter is given in the class, it returns only the
        combinations in which the *include* variable occurs. Uses the built-in function
        :py:meth:`itertools.combinations`.

        :param variables_list: The list of the variables as strings.
        :param width: The length of the combinations.
        :param use_include: If **use_include** is *False* the function will return all the possible
            combinations regardless the *self.include* parameter is set.
        :param extra_include: Here we can add extra variables (in addition to the ones in the
            *self.include* parameter) we want to be included in all of the combinations.
        :return: The list of the combinations.
        :rtype: list
        """
        if not use_include:
            return [x for x in combinations(variables_list, width)
                    if set(extra_include).issubset(set(x))]
        return [x for x in combinations(variables_list, width)
                if set(self.include).issubset(set(x))
                and not set(self.exclude).intersection(set(x))
                and set(extra_include).issubset(set(x))]

    def get_kl_divergence(self):
        """
        Returns the Kullback-Leibler divergence between the real probability distribution and the
        cherry tree approximation. The formula is the following:

        .. math::

            D(P_{orig} \\| P_{tree}) = W_{orig} - W_{tree},

        where :math:`P_{orig}` is the real distribution, :math:`P_{tree}` is the approximation
        of the distribution based on the cherry tree, :math:`W_{orig}` is the information content of
        the joint probability distribution, :math:`W_{tree}` is the weight of the cherry tree.

        :return: The Kullback-Leibler divergence of the tree.
        :rtype: float
        """
        variables = self.input_object.get_variables()
        orig_inf = self.input_object.get_inf_cont(*variables)
        tree_weight = self.junction_tree.weight
        return orig_inf - tree_weight

    def get_kl_standard_div(self):
        """
        Calculate the Kullback-Leibler divergence between the real probability distribution and the
        cherry tree probability approximations standardized to the data. The formula is the
        following:

        .. math::

            D(\\bar{P_{s}} \\| P_{s}) =
            - \\sum_{s} \\bar{P_s} \\mathrm{log}_2 \\frac{P_s}{\\bar{P_s}},

        where :math:`P_s` is the cherry tree approximation of sample realization *s* and
        :math:`\\bar{P_s}` is the real probability of sample realization *s*.

        :return: The Kullback-Leibler divergence of the cherry tree and sample.
        :rtype: float
        """
        variables = self.input_object.get_variables()
        number_of_records = self.input_object.dataframe.shape[0]
        normalizing_constant = self.get_normalizing_constant()
        kl_div = 0
        frequencies = self.input_object.get_frequency(*variables)
        for sample, number in frequencies.items():
            sample_dict = dict(zip(self.input_object.dataframe.columns, sample))
            ch_prob_norm = self.approximate_sample_without_norm(sample_dict) / normalizing_constant
            sample_prob = number / number_of_records
            for_log = ch_prob_norm / sample_prob
            final_prod = (sample_prob * math.log(for_log, 2)) if for_log else 0
            kl_div -= final_prod
        return kl_div

    def get_bic(self):
        """
        Calculate the Bayesian information criterion (BIC) of the fitted tree. The formula is the
        following:

        .. math::

            BIC = \\mathrm{log} \\hat{\\alpha} - \\frac{params}{2} \\mathrm{log}_2(n),

        where :math:`n` is the number of the samples,

        .. math::

            params = \\sum_{C} \\prod_{c\\in C} \\text{levels of } c - \\sum_{S} \\prod_{s\\in S}
            \\text{levels of } s - 1,

        and

        .. math::

            \\mathrm{log} \\hat{\\alpha} = n(W - \\sum_{A \\in V} H(A)),

        where :math:`W` is the weight of the tree, :math:`S` is the set of separators, :math:`C` is
        the set of clusters and :math:`V` is the set of the variables. The level of a variable is
        the number of unique values it can take.

        :return: The BIC of the fitted tree.
        """
        size_of_data = self.input_object.dataframe.shape[0]
        entropy = 0
        variables = self.input_object.get_variables()
        for variable in variables:
            entropy -= sum(
                [x * math.log(x, 2)
                 for x in self.input_object.dataframe[variable].value_counts() / size_of_data])
        alpha = size_of_data * (self.junction_tree.weight - entropy)
        cluster_sum = 0
        separator_sum = 0
        clust_sep_hash = self.junction_tree.get_cluster_sep_hash()
        for cluster, separator in clust_sep_hash.items():
            prod_list = [len(self.input_object.dataframe[var].unique()) for var in cluster]
            cluster_sum += functools.reduce(operator.mul, prod_list)
            prod_sep_list = [len(self.input_object.dataframe[var].unique())
                             for var in separator] if separator else [0]
            separator_sum += functools.reduce(operator.mul, prod_sep_list)
        params = cluster_sum - separator_sum - 1
        return alpha - (params / 2) * math.log(size_of_data, 2)

    def goodness_of_fit(self):
        """
        Returns the goodness of fit of the cherry tree probability distribution to the data. The
        goodness of fit of the cherry tree probability
        distribution to the data can be expressed by the Kullback-Leibler divergence (as smaller
        as better), and by the goodness of fit (GOF) which takes values between 0 and 1 (as closer
        to 1 as better the approximation is). The formula is the following:

        .. math::

            GOF_{tree} = W_{tree} / W_{orig},

        where :math:`W_{orig}` is the information content of
        the variables, :math:`W_{tree}` is the weight of the junction tree.

        :return: The goodness of fit, a value between 0 and 1.
        :rtype: float
        """
        variables = self.input_object.get_variables()
        orig_inf = self.input_object.get_inf_cont(*variables)
        tree_weight = self.junction_tree.weight
        return tree_weight / orig_inf

    def approximate_sample_without_norm(self, sample_hash: dict):
        """
        Approximate the cherry tree probability of a sample realization given in **sample_hash**
        as {variable_name: value, ...} dictionary occurs in the sample.

        :param sample_hash: The examined sample as dictionary.
        :return: The cherry tree approximation of the given sample realization.
        :rtype: float
        """
        number_of_records = self.input_object.dataframe.shape[0]
        numerator = 1
        denominator = 1
        clust_sep_hash = self.junction_tree.get_cluster_sep_hash()
        for cluster, separator in clust_sep_hash.items():
            sample_value = tuple((sample_hash[x] for x in sorted(cluster)))
            numerator *= \
                self.input_object.get_frequency(*cluster).get(sample_value, 0) / number_of_records
            if separator:
                sample_value_sep = tuple((sample_hash[x] for x in sorted(separator)))
                denominator *= self.input_object.get_frequency(*separator).get(
                    sample_value_sep, 1) / number_of_records
        return numerator / denominator

    def get_normalizing_constant(self):
        """
        Calculate the normalizing constant based on the cherry tree probabilities associated to the
        sample realizations. The normalizing constant can be fetched with the following formula:

        .. math::

            \\sum_{s\\in S} P_{approx}(s),

        where :math:`P_{approx}(s)` is the approximation of the sample row :math:`s` and :math:`S`
        is the set of all unique samples.

        :return: The normalizing constant.
        :rtype: float
        """
        variables = self.input_object.get_variables()
        dropped_df = self.input_object.dataframe[variables].drop_duplicates()
        dropped_df['sample_prob'] = dropped_df.apply(
            lambda row: self.approximate_sample_without_norm(row.to_dict()), axis=1)
        return dropped_df['sample_prob'].sum()

    def prob_realization(self, sample_hash: dict):
        """
        Approximate the probability that the sample row given in **sample_hash**
        as {variable_name: value, ...} dictionary occurs in the sample. The method uses a
        normalization constant. The normalization constant is the sum of all cherry tree
        probabilities relative to all sample realizations.

        :Example:

        >>> import pandas as pd
        >>> from cherrytree.utils.base import CherryTree, Base, Node, Star
        >>> dataframe = pd.DataFrame(data={'A': [1, 5, 2], 'B': [6, 4, 6], 'C': [7, 7, 4]})
        >>> a = Base(dataframe, 2)
        >>> junc_tree = CherryTree()
        >>> junc_tree.clusters = {Node(('A', 'B'), 1.5), Node(('B', 'C'), 0.5)}
        >>> junc_tree.stars = [Star(Node(('B',), 0), clusters=[Node(('A', 'B'), 1.5),
        ...                                                    Node(('B', 'C'), 0.5)])]
        >>> a.junction_tree = junc_tree
        >>> a.prob_realization({'A': 2, 'B': 4, 'C': 7})
        0.0
        >>> a.prob_realization({'A': 1, 'B': 6, 'C': 7})
        0.33333

        :param sample_hash: The examined sample as dictionary.
        :return: The approximation of a given sample realization. The cherry tree probability of
        occurring a realization.
        :rtype: float
        """
        return self.approximate_sample_without_norm(sample_hash) / self.get_normalizing_constant()
