import numpy as np
import pandas as pd
import unittest
from unittest.mock import MagicMock

from cherrytree.utils.base import Base, CherryTree, InputHandler, JunctionTree, Node, Star
from pandas.util.testing import assert_series_equal


data = pd.DataFrame({'Test 1': [5, 5, 1, 1, 3],
                     'Test 2': [7, 7, 1, 7, 4],
                     'Test 3': [1, 1, 1, 1, 1],
                     'Test 4': [2, 2, 1, 5, 2],
                     'Test 5': [1, 1, 1, 3, 5]})


class TestInputHandler(unittest.TestCase):
    def setUp(self):
        self.input_obj = InputHandler(data)

    def test_check_dataframe(self):
        is_empty = pd.DataFrame({'Test 1': [5.7, 5], 'Test 2': [7, pd.NaT]})
        with self.assertRaises(ValueError):
            InputHandler(is_empty)
        is_empty_2 = pd.DataFrame({'Test 1': [5.7, 5], 'Test 2': [7, np.nan]})
        with self.assertRaises(ValueError):
            InputHandler(is_empty_2)

    def test_frequency_column(self):
        self.assertTrue(self.input_obj.freq_column_name in list(self.input_obj.dataframe))
        assert_series_equal(self.input_obj.dataframe[self.input_obj.freq_column_name],
                            pd.Series([1, 1, 1, 1, 1], name=self.input_obj.freq_column_name))

    def test_get_variables(self):
        self.assertSetEqual(self.input_obj.get_variables(), {'Test 1', 'Test 2', 'Test 3', 'Test 4',
                                                             'Test 5'})

    def test_get_frequency(self):
        self.assertDictEqual(self.input_obj.get_frequency('Test 5', 'Test 3'),
                             {(1, 1): 3, (1, 3): 1, (1, 5): 1})
        self.assertDictEqual(self.input_obj.get_frequency('Test 3', 'Test 5'),
                             {(1, 1): 3, (1, 3): 1, (1, 5): 1})
        self.assertDictEqual(self.input_obj.get_frequency('Test 1', 'Test 2', 'Test 3'),
                             {(5, 7, 1): 2, (1, 1, 1): 1, (1, 7, 1): 1, (3, 4, 1): 1})
        self.assertDictEqual(self.input_obj.get_frequency('Test 1'), {5: 2, 1: 2, 3: 1})
        self.assertDictEqual(
            self.input_obj.get_frequency('Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5'),
            {(5, 7, 1, 2, 1): 2, (1, 1, 1, 1, 1): 1, (1, 7, 1, 5, 3): 1, (3, 4, 1, 2, 5): 1})

    def test_get_information_content(self):
        self.assertEqual(self.input_obj.get_inf_cont('Test 2', 'Test 3'), 0.0)
        self.assertEqual(round(self.input_obj.get_inf_cont('Test 1', 'Test 3', 'Test 5'), 5),
                         0.97095)


class TestNode(unittest.TestCase):
    def setUp(self):
        self.cluster = Node(('A', 'B', 'C'), 1.0)

    def test_contains(self):
        self.assertTrue('A' in self.cluster.name)
        self.assertTrue('D' not in self.cluster.name)


class TestStar(unittest.TestCase):
    def setUp(self):
        self.central_sep = Node(('A', 'B'), 1)
        self.cluster_1 = Node(('A', 'B', 'C'), 1)
        self.cluster_2 = Node(('A', 'B', 'D'), 1)
        self.star = Star(self.central_sep, clusters=[self.cluster_1, self.cluster_2])

        self.intersect = Star(Node(('A', 'D'), 1),
                              clusters=[Node(('A', 'C', 'D'), 1), Node(('A', 'B', 'D'), 1)])

        self.not_intersect = Star(Node(('A',), 1),
                                  clusters=[Node(('A', 'D'), 1), Node(('A', 'E'), 1)])

    def test_intersect_positive(self):
        self.assertIsNotNone(self.star.intersect(self.intersect))
        self.assertEqual(self.star.intersect(self.intersect), self.cluster_2)

    def test_intersect_negative(self):
        self.assertIsNone(self.star.intersect(self.not_intersect))

    def test_eq_positive(self):
        same_star = Star(Node(('A', 'B'), 1), clusters=[Node(('A', 'B', 'D'), 1),
                                                        Node(('A', 'B', 'C'), 1)])
        self.assertTrue(self.star == same_star)

    def test_eq_negative(self):
        same_star = Star(Node(('A', 'B'), 1), clusters=[Node(('A', 'B', 'D'), 1),
                                                        Node(('A', 'B', 'C'), 1),
                                                        Node(('A', 'B', 'E'), 1)])
        self.assertFalse(self.star == same_star)

    def test_str(self):
        self.assertEqual(
            str(self.star), str(('A', 'B')) + '**' + str([('A', 'B', 'C'), ('A', 'B', 'D')]))


class TestBase(unittest.TestCase):
    def setUp(self):
        self.base = Base(data, 3)
        junc_tree = JunctionTree()
        junc_tree.stars = [Star(Node(('Test 1', 'Test 2'), 0.5),
                                clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                                          Node(('Test 1', 'Test 2', 'Test 4'), 2),
                                          Node(('Test 1', 'Test 2', 'Test 5'), 3)])]
        junc_tree.clusters = [Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 1', 'Test 2', 'Test 4'), 2),
                              Node(('Test 1', 'Test 2', 'Test 5'), 3)]
        self.base.junction_tree = junc_tree

    def test_check_width_positive(self):
        self.assertEqual(self.base.cluster_width, 3)

    def test_check_width_negative(self):
        with self.assertRaises(ValueError):
            Base(data, 6)

        with self.assertRaises(ValueError):
            Base(data, 1)

        with self.assertRaises(ValueError):
            Base(data, 1.6)

    def test_check_include_positive(self):
        include_good = Base(data, 3, include=('Test 1', 'Test 2'))
        self.assertEqual(include_good.include, ('Test 1', 'Test 2'))

    def test_check_include_negative(self):
        with self.assertRaises(ValueError):
            Base(data, 3, include=('Test 1', 'Test 2', 'Test 3'))

    def test_check_exclude_positive(self):
        exclude_good = Base(data, 3, exclude=('Test 1', 'Test 2'))
        self.assertEqual(exclude_good.exclude, ('Test 1', 'Test 2'))

    def test_check_exclude_negative(self):
        with self.assertRaises(ValueError):
            Base(data, 3, exclude=('Test 1', 'Test 2', 'Test 3'))

    def test_check_start_positive(self):
        start_good = Base(data, 3, start_from=('Test 1', 'Test 2', 'Test 3'))
        self.assertEqual(start_good.start_from, ('Test 1', 'Test 2', 'Test 3'))

    def test_check_start_negative(self):
        with self.assertRaises(ValueError):
            Base(data, 3, start_from=('Test 1', 'Test 2', 'Test 3', 'Test 4'))

    def test_check_include_exclude_positive(self):
        both = Base(data, 3, include=('Test 1', 'Test 2'), exclude=('Test 3', 'Test 4'))
        self.assertEqual(both.include, ('Test 1', 'Test 2'))
        self.assertEqual(both.exclude, ('Test 3', 'Test 4'))

    def test_include_exclude_negative(self):
        with self.assertRaises(ValueError):
            Base(data, 3, include=('Test 1', 'Test 2'), exclude=('Test 1', 'Test 4'))

    def test_combinations_base(self):
        base_3 = Base(data, 3)
        self.assertListEqual(
            list(base_3.get_combinations(['Test 1', 'Test 4', 'Test 5'], 2)),
            [('Test 1', 'Test 4'), ('Test 1', 'Test 5'), ('Test 4', 'Test 5')]
        )

    def test_combinations_include(self):
        base_3_include = Base(data, 3, include=('Test 2',))
        self.assertListEqual(
            list(base_3_include.get_combinations(['Test 1', 'Test 2', 'Test 5'], 2)),
            [('Test 1', 'Test 2'), ('Test 2', 'Test 5')]
        )
        self.assertListEqual(
            list(base_3_include.get_combinations(['Test 1', 'Test 2', 'Test 5'],
                                                      2, use_include=False)),
            [('Test 1', 'Test 2'), ('Test 1', 'Test 5'), ('Test 2', 'Test 5')]
        )

    def test_combinations_exclude(self):
        base_3_exclude = Base(data, 3, exclude=('Test 2',))
        self.assertListEqual(
            list(base_3_exclude.get_combinations(['Test 1', 'Test 2', 'Test 5'], 2)),
            [('Test 1', 'Test 5')]
        )
        self.assertListEqual(
            list(base_3_exclude.get_combinations(['Test 1', 'Test 2', 'Test 5'], 2,
                                                 use_include=False)),
            [('Test 1', 'Test 2'), ('Test 1', 'Test 5'),
             ('Test 2', 'Test 5')]
        )

    def test_combinations_include_exclude(self):
        base_3_inexclude = Base(data, 3, exclude=('Test 2',), include=('Test 1',))
        self.assertListEqual(
            list(base_3_inexclude.get_combinations(
                ['Test 1', 'Test 2', 'Test 3', 'Test 5'], 2)),
            [('Test 1', 'Test 3'), ('Test 1', 'Test 5')]
        )

    def test_combinations_extra_include(self):
        self.assertListEqual(
            list(self.base.get_combinations(
                ['Test 1', 'Test 2', 'Test 3', 'Test 5'], 3, extra_include=('Test 2', 'Test 3'))),
            [('Test 1', 'Test 2', 'Test 3'), ('Test 2', 'Test 3', 'Test 5')]
        )
        self.assertListEqual(
            list(self.base.get_combinations(
                ['Test 1', 'Test 2', 'Test 3'], 2, use_include=False,
                extra_include=('Test 2', ))),
            [('Test 1', 'Test 2'), ('Test 2', 'Test 3')]
        )

    def test_combinations_start_include(self):
        with self.assertRaises(ValueError):
            Base(data, 3, include=('Test 2', 'Test 3'),
                 start_from=('Test 1', 'Test 5', 'Test 6'))

    def test_kl_divergence(self):
        information = {('Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5'): 2}
        self.base.input_object.information = information
        self.base.junction_tree.weight = 0.5
        self.assertEqual(self.base.get_kl_divergence(), 1.5)

    def test_kl_std_div(self):
        self.assertEqual(self.base.get_kl_standard_div(), 0)

    def test_bic(self):
        self.assertEqual(round(self.base.get_bic(), 6), -79.256317)

    def test_gof(self):
        information = {('Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5'): 2}
        self.base.input_object.information = information
        self.base.junction_tree.weight = 0.5
        self.assertEqual(self.base.goodness_of_fit(), 0.25)

    def test_approx_sample_wout_norm(self):
        self.assertEqual(
            self.base.approximate_sample_without_norm(
                {'Test 1': 3, 'Test 2': 7, 'Test 3': 1, 'Test 4': 2, 'Test 5': 1}), 0)
        self.assertEqual(
            self.base.approximate_sample_without_norm(
                {'Test 1': 5, 'Test 2': 7, 'Test 3': 1, 'Test 4': 2, 'Test 5': 1}), 0.4)
        self.assertEqual(
            self.base.approximate_sample_without_norm(
                {'Test 1': 1, 'Test 2': 7, 'Test 3': 1, 'Test 4': 5, 'Test 5': 3}), 0.2)

    def test_norm_const(self):
        self.assertEqual(self.base.get_normalizing_constant(), 1)

    def test_prob_realization(self):
        self.assertEqual(self.base.prob_realization(
            {'Test 1': 3, 'Test 2': 7, 'Test 3': 1, 'Test 4': 2, 'Test 5': 1}), 0)
        self.assertEqual(self.base.prob_realization(
            {'Test 1': 5, 'Test 2': 7, 'Test 3': 1, 'Test 4': 2, 'Test 5': 1}), 0.4)
        self.assertEqual(self.base.prob_realization(
            {'Test 1': 1, 'Test 2': 7, 'Test 3': 1, 'Test 4': 5, 'Test 5': 3}), 0.2)


class TestJunctionTree(unittest.TestCase):
    def setUp(self):
        self.cluster_1 = Node(('Test 1', 'Test 4', 'Test 5'), 1)
        self.cluster_2 = Node(('Test 1', 'Test 3', 'Test 5'), 1)
        self.central_1 = Node(('Test 1', 'Test 5'), 1)
        self.star_1 = Star(self.central_1, clusters=[self.cluster_1, self.cluster_2])
        self.cluster_3 = Node(('Test 1', 'Test 2', 'Test 3'), 1)
        self.central_2 = Node(('Test 1', 'Test 3'), 1)
        self.star_2 = Star(self.central_2, clusters=[self.cluster_2, self.cluster_3])
        self.junction_tree = JunctionTree()
        self.junction_tree.stars = [self.star_1, self.star_2]
        self.junction_tree.clusters = [self.cluster_1.name, self.cluster_2.name, self.cluster_3.name]
        self.junction_tree.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5'}
        self.junction_tree.weight = 1

    def test_cluster_sep_hash(self):
        self.assertEqual(self.junction_tree.get_cluster_sep_hash(),
                         {('Test 1', 'Test 4', 'Test 5'): None,
                          ('Test 1', 'Test 3', 'Test 5'): ('Test 1', 'Test 5'),
                          ('Test 1', 'Test 2', 'Test 3'): ('Test 1', 'Test 3')})

    def test_check_running_intersection_positive(self):
        self.assertIsNone(self.junction_tree.check_running_intersection())

    def test_check_running_intersection_negative(self):
        cluster_1 = Node(('Test 1', 'Test 2', 'Test 3'), 1)
        cluster_2 = Node(('Test 2', 'Test 4'), 1)
        central_1 = Node(('Test 2',), 1)
        star_1 = Star(central_1, clusters=[cluster_1, cluster_2])
        cluster_3 = Node(('Test 1', 'Test 4', 'Test 5'), 1)
        central_2 = Node(('Test 4',), 1)
        star_2 = Star(central_2, clusters=[cluster_2, cluster_3])
        junction_tree = JunctionTree()
        junction_tree.stars = [star_1, star_2]
        junction_tree.clusters = [cluster_1.name, cluster_2.name, cluster_3.name]
        junction_tree.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4', 'Test 5'}
        with self.assertRaises(ValueError):
            junction_tree.check_running_intersection()

    def test_get_relevant_variables(self):
        self.assertEqual(self.junction_tree.get_relevant_variables('Test 4'),
                         {'Test 1', 'Test 5'})
        self.assertEqual(self.junction_tree.get_relevant_variables('Test 1'),
                         {'Test 2', 'Test 3', 'Test 4', 'Test 5'})

    def test_width(self):
        cluster_1 = Node(('Test 1', 'Test 2', 'Test 3'), 1)
        cluster_2 = Node(('Test 2', 'Test 4'), 1)
        junction_tree = JunctionTree()
        junction_tree.clusters = [cluster_1.name, cluster_2.name]
        self.assertEqual(junction_tree.get_width(), 3)
        junction_tree.clusters = []
        self.assertEqual(junction_tree.get_width(), 0)

    def test_update_star(self):
        new_cluster = Node(('Test 1', 'Test 5', 'Test 6'), 1)
        self.junction_tree.update_star_with(self.star_1, new_cluster)
        self.assertTrue(new_cluster in self.star_1.clusters)
        self.assertTrue(('Test 1', 'Test 5', 'Test 6') in self.junction_tree.clusters)
        self.assertEqual(self.junction_tree.weight, 1)
        self.assertTrue({'Test 1', 'Test 5', 'Test 6'}.issubset(self.junction_tree.variables))

    def test_update_star_already_in(self):
        new_cluster = Node(('Test 1', 'Test 4', 'Test 5'), 1)
        self.junction_tree.update_star_with(self.star_1, new_cluster)
        self.assertTrue(new_cluster.name in [clust.name for clust in self.star_1.clusters])
        self.assertTrue(('Test 1', 'Test 4', 'Test 5') in self.junction_tree.clusters)
        self.assertEqual(self.junction_tree.weight, 1)
        self.assertTrue({'Test 1', 'Test 4', 'Test 5'}.issubset(self.junction_tree.variables))

    def test_remove_from_star(self):
        self.junction_tree.remove_from_star(self.star_1, self.cluster_1)
        self.assertTrue(self.cluster_1 not in self.star_1.clusters)
        self.assertTrue(('Test 1', 'Test 4', 'Test 5') not in self.junction_tree.clusters)
        self.assertEqual(self.junction_tree.weight, 1)
        self.assertFalse({'Test 1', 'Test 5', 'Test 6'}.issubset(self.junction_tree.variables))

    def test_remove_from_star_not_in_star(self):
        cluster_not_in_star = Node(('Test 1', 'Test 2', 'Test 3'), 1)
        self.junction_tree.remove_from_star(self.star_1, cluster_not_in_star)
        self.assertFalse(cluster_not_in_star.name in [clust.name for clust in self.star_1.clusters])
        self.assertEqual(self.junction_tree.weight, 1)

    def test_get_star_by_central_positive(self):
        self.assertEqual(self.junction_tree.get_star_by_central(('Test 1', 'Test 3')),
                         self.star_2)

    def test_get_star_by_central_negative(self):
        with self.assertRaises(ValueError):
            self.junction_tree.get_star_by_central(('Test 10',))

    def test_get_neighbor_positive(self):
        self.assertEqual(self.junction_tree._get_neighbor_star_cluster(self.central_1),
                         (self.star_1, self.cluster_1))

    def test_get_neighbor_negative(self):
        self.assertIsNone(self.junction_tree._get_neighbor_star_cluster(Node(('Test 6',), 1)))

    def test_extend_star(self):
        new_star = Star(Node(('Test 3',), 1), clusters=[Node(('Test 3', 'Test 6'), 1),
                                                        Node(('Test 3', 'Test 7'), 1)])
        self.junction_tree.extend_with_star(new_star)
        self.assertTrue(new_star in self.junction_tree.stars)
        self.assertTrue({('Test 3', 'Test 6'), ('Test 3', 'Test 7')} < set(self.junction_tree.clusters))
        self.assertTrue({'Test 3', 'Test 6', 'Test 7'} < self.junction_tree.variables)
        self.assertEqual(self.junction_tree.weight, 2)

    def test_extend_star_already_in(self):
        self.junction_tree.extend_with_star(self.star_1)
        self.assertTrue(self.star_1 in self.junction_tree.stars)
        self.assertEqual(self.junction_tree.weight, 1)

    def test_add_star_no_sep(self):
        new_star = Star(Node(('Test 3',), 1), clusters=[Node(('Test 3', 'Test 6'), 1),
                                                        Node(('Test 3', 'Test 7'), 1)])
        self.junction_tree.extend_with_star = MagicMock()
        self.junction_tree.add_star(new_star)
        assert self.junction_tree.extend_with_star.called

    def test_add_star_empty_list(self):
        empty_tree = JunctionTree()
        new_star = Star(Node(('Test 3',), 1), clusters=[Node(('Test 3', 'Test 6'), 1),
                                                        Node(('Test 3', 'Test 7'), 1)])
        empty_tree.add_star(new_star)
        self.assertTrue(new_star in empty_tree.stars)
        self.assertTrue(
            {('Test 3', 'Test 6'), ('Test 3', 'Test 7')}.issubset(set(empty_tree.clusters)))
        self.assertTrue({'Test 3', 'Test 6', 'Test 7'}.issubset(empty_tree.variables))
        self.assertEqual(empty_tree.weight, 1)

    def test_add_star_case_1(self):
        junction = JunctionTree()
        star = Star(clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3'}
        junction.weight = 1
        new_star = Star(clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 1)])
        central = Node(('Test 2', 'Test 3'), 1)
        junction.add_star(new_star, sep=central)
        self.assertEqual(star.central_sep.name, central.name)
        self.assertEqual(star.central_sep.weight, central.weight)
        self.assertTrue(new_star.clusters[0] in star.clusters)
        self.assertTrue(set(new_star.clusters[0].name) < junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star not in junction.stars)

    def test_add_star_case_2(self):
        junction = JunctionTree()
        star = Star(clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3'}
        junction.weight = 1
        new_star = Star(Node(('Test 2', 'Test 3'), 1),
                        clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 1),
                                  Node(('Test 2', 'Test 3', 'Test 5'), 1)])
        central = Node(('Test 2', 'Test 3'), 1)
        junction.add_star(new_star, sep=central)
        self.assertEqual(star.central_sep.name, central.name)
        self.assertEqual(star.central_sep.weight, central.weight)
        self.assertTrue(set(new_star.clusters) < set(star.clusters))
        self.assertTrue({'Test 4', 'Test 5'} < junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star not in junction.stars)

    def test_add_star_case_3(self):
        junction = JunctionTree()
        star = Star(clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3'}
        junction.weight = 1
        new_star = Star(Node(('Test 2', 'Test 5'), 1),
                        clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 1),
                                  Node(('Test 1', 'Test 2', 'Test 5'), 1)])
        central = Node(('Test 2', 'Test 3'), 1)
        junction.add_star(new_star, sep=central)
        self.assertEqual(star.central_sep.name, central.name)
        self.assertEqual(star.central_sep.weight, central.weight)
        self.assertTrue(new_star.clusters[0] in star.clusters)
        self.assertTrue({'Test 4', 'Test 5'} < junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star in junction.stars)

    def test_add_star_case_4(self):
        junction = JunctionTree()
        star = Star(Node(('Test 1', 'Test 2'), 1),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 1', 'Test 2', 'Test 4'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3'), ('Test 1', 'Test 2', 'Test 4')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        junction.weight = 1
        new_star = Star(clusters=[Node(('Test 1', 'Test 2', 'Test 5'), 1)])
        central = Node(('Test 1', 'Test 2'), 1)
        junction.add_star(new_star, sep=central)
        self.assertEqual(star.central_sep.name, central.name)
        self.assertEqual(star.central_sep.weight, central.weight)
        self.assertTrue(new_star.clusters[0] in star.clusters)
        self.assertTrue('Test 5' in junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star not in junction.stars)

    def test_add_star_case_5(self):
        junction = JunctionTree()
        star = Star(Node(('Test 1', 'Test 2'), 1),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 1', 'Test 2', 'Test 4'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3'), ('Test 1', 'Test 2', 'Test 4')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        junction.weight = 1
        new_star = Star(clusters=[Node(('Test 1', 'Test 4', 'Test 5'), 1)])
        central = Node(('Test 1', 'Test 4'), 1)
        junction.add_star(new_star, sep=central)
        self.assertEqual(new_star.central_sep.name, central.name)
        self.assertEqual(new_star.central_sep.weight, central.weight)
        self.assertTrue(star.clusters[1] in new_star.clusters)
        self.assertTrue('Test 5' in junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star in junction.stars)

    def test_add_star_case_6(self):
        junction = JunctionTree()
        star = Star(Node(('Test 1', 'Test 2'), 1),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 1', 'Test 2', 'Test 4'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3'), ('Test 1', 'Test 2', 'Test 4')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        junction.weight = 1
        new_star = Star(Node(('Test 1', 'Test 2'), 1),
                        clusters=[Node(('Test 1', 'Test 2', 'Test 5'), 1),
                                  Node(('Test 1', 'Test 2', 'Test 6'), 1)])
        central = Node(('Test 1', 'Test 2'), 1)
        junction.add_star(new_star, sep=central)
        self.assertTrue(set(new_star.clusters) < set(star.clusters))
        self.assertTrue({'Test 5', 'Test 6'} < junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star not in junction.stars)

    def test_add_star_case_7(self):
        junction = JunctionTree()
        star = Star(Node(('Test 1', 'Test 2'), 1),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 1', 'Test 2', 'Test 4'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3'), ('Test 1', 'Test 2', 'Test 4')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        junction.weight = 1
        new_star = Star(Node(('Test 1', 'Test 5'), 1),
                        clusters=[Node(('Test 1', 'Test 2', 'Test 5'), 1),
                                  Node(('Test 1', 'Test 5', 'Test 6'), 1)])
        central = Node(('Test 1', 'Test 2'), 1)
        junction.add_star(new_star, sep=central)
        self.assertTrue(new_star.clusters[0] in star.clusters)
        self.assertTrue({'Test 5', 'Test 6'} < junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star in junction.stars)

    def test_add_star_case_8(self):
        junction = JunctionTree()
        star = Star(Node(('Test 1', 'Test 2'), 1),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 1', 'Test 2', 'Test 4'), 1)])
        junction.stars.append(star)
        junction.clusters = [('Test 1', 'Test 2', 'Test 3'), ('Test 1', 'Test 2', 'Test 4')]
        junction.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        junction.weight = 1
        new_star = Star(Node(('Test 1', 'Test 5'), 1),
                        clusters=[Node(('Test 1', 'Test 4', 'Test 5'), 1),
                                  Node(('Test 1', 'Test 5', 'Test 6'), 1)])
        central = Node(('Test 1', 'Test 4'), 1)
        junction.add_star(new_star, sep=central)
        connect_star = Star(Node(('Test 1', 'Test 4'), 1),
                            clusters=[Node(('Test 1', 'Test 4', 'Test 5'), 1),
                                      Node(('Test 1', 'Test 2', 'Test 4'), 1)])
        self.assertTrue(connect_star in junction.stars)
        self.assertTrue({'Test 5', 'Test 6'} < junction.variables)
        self.assertEqual(junction.weight, 1)
        self.assertTrue(new_star in junction.stars)

    def test_repr(self):
        self.assertEqual(
            str(self.junction_tree),
            str({'stars': self.junction_tree.stars, 'weight': 1}))

    def test_format_string(self):
        self.assertEqual(self.junction_tree.format_str("('Test 1', 'Test 2')"), "Test 1, Test 2")
        self.assertEqual(self.junction_tree.format_str("{'Test 1', 'Test 2'}"), "Test 1, Test 2")


class TestCherryTree(unittest.TestCase):
    def setUp(self):
        self.cluster_1 = Node(('Test 1', 'Test 4', 'Test 5'), 1)
        self.cluster_2 = Node(('Test 1', 'Test 3', 'Test 5'), 1)
        self.central_1 = Node(('Test 1', 'Test 5'), 1)
        self.star_1 = Star(self.central_1, clusters=[self.cluster_1, self.cluster_2])
        self.junction_tree = CherryTree()
        self.junction_tree.stars = [self.star_1]
        self.junction_tree.clusters = [self.cluster_1.name, self.cluster_2.name]
        self.junction_tree.variables = {'Test 1', 'Test 3', 'Test 4', 'Test 5'}
        self.junction_tree.weight = 1

    def test_width(self):
        self.assertEqual(self.junction_tree.get_width(), 3)
        self.junction_tree.clusters = []
        self.assertEqual(self.junction_tree.get_width(), 0)


if __name__ == "__main__":
    unittest.main()
