import unittest

import pandas as pd
from pandas.util.testing import assert_frame_equal

from cherrytree.utils.discretize import discretize_digitize, discretize_quantile


class TestDiscretize(unittest.TestCase):
    def setUp(self):
        self.df = pd.DataFrame([
            {'Test 1': 5.456, 'Test 2': 7.623, 'Test 3': 1.561, 'Test 4': 2.91, 'Test 5': 1.14},
            {'Test 1': 5, 'Test 2': 7.23, 'Test 3': 1.56, 'Test 4': -2, 'Test 5': 1},
            {'Test 1': 1.2306, 'Test 2': -1.68, 'Test 3': 1.892, 'Test 4': -1.57, 'Test 5': 1},
            {'Test 1': 1.23468, 'Test 2': 7.92, 'Test 3': 1, 'Test 4': 5.5, 'Test 5': 3.67},
            {'Test 1': 3.457, 'Test 2': 4.34, 'Test 3': 1.56, 'Test 4': 2.82, 'Test 5': 5.1242},
        ])

    def test_digitize(self):
        result = pd.DataFrame([
            {'Test 1': 3, 'Test 2': 3, 'Test 3': 1.561, 'Test 4': 2.91, 'Test 5': 1.14},
            {'Test 1': 2, 'Test 2': 3, 'Test 3': 1.56, 'Test 4': -2, 'Test 5': 1},
            {'Test 1': 2, 'Test 2': 0, 'Test 3': 1.892, 'Test 4': -1.57, 'Test 5': 1},
            {'Test 1': 2, 'Test 2': 3, 'Test 3': 1, 'Test 4': 5.5, 'Test 5': 3.67},
            {'Test 1': 2, 'Test 2': 2, 'Test 3': 1.56, 'Test 4': 2.82, 'Test 5': 5.1242},
        ])
        discretize_digitize(self.df, ['Test 1', 'Test 2'], [0, 1.2, 5.3, 8])
        assert_frame_equal(self.df, result)

    def test_quantile(self):
        result_equal = pd.DataFrame([
            {'Test 1': 5.456, 'Test 2': 7.623, 'Test 3': 1.561, 'Test 4': 1, 'Test 5': 1.14},
            {'Test 1': 5, 'Test 2': 7.23, 'Test 3': 1.56, 'Test 4': 0, 'Test 5': 1},
            {'Test 1': 1.2306, 'Test 2': -1.68, 'Test 3': 1.892, 'Test 4': 0, 'Test 5': 1},
            {'Test 1': 1.23468, 'Test 2': 7.92, 'Test 3': 1, 'Test 4': 1, 'Test 5': 3.67},
            {'Test 1': 3.457, 'Test 2': 4.34, 'Test 3': 1.56, 'Test 4': 0, 'Test 5': 5.1242},
        ])
        discretize_quantile(self.df, ['Test 4'], 2)
        assert_frame_equal(self.df, result_equal)
        result_array = pd.DataFrame([
            {'Test 1': 2, 'Test 2': 1, 'Test 3': 1.561, 'Test 4': 1, 'Test 5': 1.14},
            {'Test 1': 1, 'Test 2': 1, 'Test 3': 1.56, 'Test 4': 0, 'Test 5': 1},
            {'Test 1': 0, 'Test 2': 0, 'Test 3': 1.892, 'Test 4': 0, 'Test 5': 1},
            {'Test 1': 0, 'Test 2': 2, 'Test 3': 1, 'Test 4': 1, 'Test 5': 3.67},
            {'Test 1': 1, 'Test 2': 0, 'Test 3': 1.56, 'Test 4': 0, 'Test 5': 5.1242},
        ])
        discretize_quantile(self.df, ['Test 1', 'Test 2'], [0, .25, .75, 1])
        assert_frame_equal(self.df, result_array)


if __name__ == "__main__":
    unittest.main()
