import networkx as nx
import pandas as pd
from typing import Iterable
from unittest import TestCase, main

from cherrytree.algorithms.chow_liu_tree import ChowLiuTreeAlgorithm
from cherrytree.algorithms.cherry import CherryTreeAlgorithm
from cherrytree.algorithms.improved_cherry import ImprovedCherryTreeAlgorithm
from cherrytree.algorithms.sequential_cherry import SequentialCherryTreeAlgorithm

from cherrytree.utils.base import CherryTree, Node, Star, JunctionTree


data = pd.DataFrame({'Test 1': [5, 5, 1, 1, 3],
                     'Test 2': [7, 7, 1, 7, 4],
                     'Test 3': [1, 1, 1, 1, 1],
                     'Test 4': [2, 2, 1, 5, 2]})


class TestChowLiu(TestCase):
    def setUp(self):
        self.algo = ChowLiuTreeAlgorithm(data)
        information = {('Test 1', 'Test 2'): 0.1, ('Test 1', 'Test 3'): 0.2,
                       ('Test 1', 'Test 4'): 0.3, ('Test 2', 'Test 3'): 0.4,
                       ('Test 2', 'Test 4'): 0.5, ('Test 3', 'Test 4'): 0.6,
                       'Test 1': 0, 'Test 2': 0, 'Test 3': 0, 'Test 4': 0}
        self.algo.input_object.information = information

    def test_build_init_graph(self):
        built_graph = self.algo.build_init_graph()
        self.assertEqual(
            set([tuple(sorted(edge)) for edge in built_graph.edges]),
            {('Test 1', 'Test 2'), ('Test 1', 'Test 3'), ('Test 1', 'Test 4'), ('Test 2', 'Test 3'),
             ('Test 2', 'Test 4'), ('Test 3', 'Test 4')}
        )
        for edge in built_graph.edges:
            self.assertEqual(built_graph.get_edge_data(*edge)['weight'],
                             self.algo.input_object.get_inf_cont(*edge))
        self.assertTrue(isinstance(built_graph, nx.Graph))

    def test_max_spanning_tree(self):
        base_graph = nx.Graph()
        for edge, weight in [(('Test 1', 'Test 2'), 1), (('Test 1', 'Test 3'), 2),
                             (('Test 1', 'Test 4'), 3), (('Test 2', 'Test 3'), 4),
                             (('Test 2', 'Test 4'), 5), (('Test 3', 'Test 4'), 5)]:
            base_graph.add_edge(*edge, weight=weight)
        max_edges = {('Test 1', 'Test 4'): 3, ('Test 3', 'Test 4'): 5, ('Test 2', 'Test 4'): 5}
        max_graph = self.algo.maximum_spanning_tree(base_graph)
        for edge in max_graph.edges:
            self.assertTrue(tuple(sorted(edge)) in max_edges)
            self.assertEqual(max_graph.get_edge_data(*edge)['weight'],
                             max_edges.get(tuple(sorted(edge))))
        self.assertTrue(isinstance(max_graph, nx.Graph))

    def test_kruskal_mst(self):
        base_graph = nx.Graph()
        for edge, weight in [(('Test 1', 'Test 2'), 1), (('Test 1', 'Test 3'), 2),
                             (('Test 1', 'Test 4'), 3), (('Test 2', 'Test 3'), 4),
                             (('Test 2', 'Test 4'), 5), (('Test 3', 'Test 4'), 5)]:
            base_graph.add_edge(*edge, weight=weight)
        max_graph = self.algo.kruskal_mst_edges(base_graph)
        max_edges = {('Test 1', 'Test 4'): 3, ('Test 3', 'Test 4'): 5, ('Test 2', 'Test 4'): 5}
        for edge in max_graph:
            self.assertTrue(tuple(sorted((edge[0], edge[1]))) in max_edges)
            self.assertEqual(edge[2]['weight'], max_edges.get(tuple(sorted((edge[0], edge[1])))))
        self.assertTrue(isinstance(max_graph, Iterable))

    def test_build_cherry(self):
        built_tree = self.algo.build_cherry_tree()
        cherry = CherryTree()
        star = Star(Node(('Test 4', ), 0), clusters=[Node(('Test 3', 'Test 4'), 0.6),
                                                     Node(('Test 1', 'Test 4'), 0.3),
                                                     Node(('Test 2', 'Test 4'), 0.5)])
        cherry.stars.append(star)
        cherry.clusters = [('Test 1', 'Test 4'), ('Test 2', 'Test 4'), ('Test 3', 'Test 4')]
        cherry.weight = 1.4
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        self.assertEqual(built_tree, cherry)


class TestCherry(TestCase):
    def setUp(self):
        self.algo = CherryTreeAlgorithm(data, 3)
        self.algo_without_multiproc = CherryTreeAlgorithm(data, 3, use_multiproc=False)
        information = {('Test 1', 'Test 2', 'Test 3'): 1, ('Test 1', 'Test 2', 'Test 4'): 2,
                       ('Test 1', 'Test 3', 'Test 4'): 3, ('Test 2', 'Test 3', 'Test 4'): 4,
                       ('Test 1', 'Test 2'): 0.5, ('Test 1', 'Test 3'): 0.5,
                       ('Test 1', 'Test 4'): 0.4, ('Test 2', 'Test 3'): 0.6,
                       ('Test 2', 'Test 4'): 0.7, ('Test 3', 'Test 4'): 0.2,
                       'Test 1': 0, 'Test 2': 0, 'Test 3': 0, 'Test 4': 0,
                       ('Test 1', 'Test 2', 'Test 3', 'Test 4'): 10}
        self.algo.input_object.information = information
        self.algo_without_multiproc.input_object.information = information
        self.algo_include = CherryTreeAlgorithm(data, 3, include=('Test 1', 'Test 2'))
        self.algo_include.input_object.information = information
        self.algo_exclude = CherryTreeAlgorithm(data, 3, exclude=('Test 3',))
        self.algo_exclude.input_object.information = information
        self.algo_start = CherryTreeAlgorithm(data, 3, start_from=('Test 1', 'Test 3'))
        self.algo_start.input_object.information = information
        self.algo_start_include = CherryTreeAlgorithm(data, 3, start_from=('Test 1', 'Test 3'),
                                                      include=('Test 2',))
        self.algo_start_include.input_object.information = information
        self.algo_small = CherryTreeAlgorithm(data, 2)
        self.algo_small.input_object.information = information
        self.algo_small_start = CherryTreeAlgorithm(data, 2, start_from=('Test 3', 'Test 4'))
        self.algo_small_start.input_object.information = information

    def test_get_root(self):
        root = Star(clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 4)])
        self.assertEqual(self.algo.get_root(), root)

    def test_get_root_without_multiproc(self):
        root = Star(clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 4)])
        self.assertEqual(self.algo_without_multiproc.get_root(), root)

    def test_next_cluster(self):
        root = Star(clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 4)])
        self.algo.junction_tree.variables = {'Test 2', 'Test 3', 'Test 4'}
        self.algo.junction_tree.stars = [root]
        self.algo.junction_tree.clusters = [('Test 2', 'Test 3', 'Test 4')]
        self.algo.weight = 4
        next_clust, cluster_in, sep = self.algo.get_next_cluster()
        self.assertEqual(next_clust.name, ('Test 1', 'Test 3', 'Test 4'))
        self.assertEqual(cluster_in, ('Test 2', 'Test 3', 'Test 4'))
        self.assertEqual(sep.name, ('Test 3', 'Test 4'))

    def test_build_cherry(self):
        cherry = CherryTree()
        star = Star(Node(('Test 3', 'Test 4'), 0.2),
                    clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 4),
                              Node(('Test 1', 'Test 3', 'Test 4'), 3)])
        cherry.stars.append(star)
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 2', 'Test 3', 'Test 4'), ('Test 1', 'Test 3', 'Test 4')]
        cherry.weight = 6.8
        self.assertEqual(self.algo.build_cherry_tree(), cherry)

    def test_include(self):
        cherry = CherryTree()
        star = Star(Node(('Test 1', 'Test 2'), 0.5),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 4'), 2),
                              Node(('Test 1', 'Test 2', 'Test 3'), 1)])
        cherry.stars.append(star)
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 1', 'Test 2', 'Test 4'), ('Test 1', 'Test 2', 'Test 3')]
        cherry.weight = 2.5
        self.assertEqual(self.algo_include.build_cherry_tree(), cherry)

    def test_exclude(self):
        cherry = CherryTree()
        star = Star(Node(('Test 2', 'Test 4'), 0.7),
                    clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 4),
                              Node(('Test 1', 'Test 2', 'Test 4'), 2)])
        cherry.stars.append(star)
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 2', 'Test 3', 'Test 4'), ('Test 1', 'Test 2', 'Test 4')]
        cherry.weight = 5.3
        self.assertEqual(self.algo_exclude.build_cherry_tree(), cherry)

    def test_start_from(self):
        cherry = CherryTree()
        star = Star(Node(('Test 3', 'Test 4'), 0.2),
                    clusters=[Node(('Test 1', 'Test 3', 'Test 4'), 3),
                              Node(('Test 2', 'Test 3', 'Test 4'), 4)])
        cherry.stars.append(star)
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 1', 'Test 3', 'Test 4'), ('Test 2', 'Test 3', 'Test 4')]
        cherry.weight = 6.8
        self.assertEqual(self.algo_start.build_cherry_tree(), cherry)

    def test_start_include(self):
        cherry = CherryTree()
        star = Star(Node(('Test 2', 'Test 3'), 0.6),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 2', 'Test 3', 'Test 4'), 4)])
        cherry.stars.append(star)
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 1', 'Test 2', 'Test 3'), ('Test 2', 'Test 3', 'Test 4')]
        cherry.weight = 4.4
        self.assertEqual(self.algo_start_include.build_cherry_tree(), cherry)

    def test_separator_in_tree(self):
        cherry = CherryTree()
        star = Star(Node(('Test 2', ), 0),
                    clusters=[Node(('Test 2', 'Test 4'), 0.7),
                              Node(('Test 2', 'Test 3'), 0.6),
                              Node(('Test 1', 'Test 2'), 0.5)])
        cherry.stars.append(star)
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 2', 'Test 4'), ('Test 2', 'Test 3'), ('Test 1', 'Test 2')]
        cherry.weight = 1.8
        self.assertEqual(self.algo_small.build_cherry_tree(), cherry)

    def test_separator_not_in_tree(self):
        cherry = CherryTree()
        star = Star(Node(('Test 4',), 0),
                    clusters=[Node(('Test 3', 'Test 4'), 0.2),
                              Node(('Test 2', 'Test 4'), 0.7)])
        star_new = Star(Node(('Test 3',), 0),
                        clusters=[Node(('Test 1', 'Test 3'), 0.5),
                                  Node(('Test 3', 'Test 4'), 0.2)])
        cherry.stars.extend([star, star_new])
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 3', 'Test 4'), ('Test 2', 'Test 4'), ('Test 1', 'Test 3')]
        cherry.weight = 1.4
        self.assertEqual(self.algo_small_start.build_cherry_tree(), cherry)


class TestImproved(TestCase):
    def setUp(self):
        self.algo = ImprovedCherryTreeAlgorithm(data, 3)
        information = {('Test 1', 'Test 2', 'Test 3'): 1, ('Test 1', 'Test 2', 'Test 4'): 2,
                       ('Test 1', 'Test 3', 'Test 4'): 3, ('Test 2', 'Test 3', 'Test 4'): 4,
                       ('Test 1', 'Test 2'): 0.5, ('Test 1', 'Test 3'): 0.5,
                       ('Test 1', 'Test 4'): 0.4, ('Test 2', 'Test 3'): 0.6,
                       ('Test 2', 'Test 4'): 0.7, ('Test 3', 'Test 4'): 0.2,
                       'Test 1': 0, 'Test 2': 0, 'Test 3': 0, 'Test 4': 0,
                       ('Test 1', 'Test 2', 'Test 3', 'Test 4'): 10}
        self.algo.input_object.information = information

    def test_bud_weight(self):
        self.assertEqual(self.algo._calculate_bud_weight(('Test 1', 'Test 2', 'Test 3'),
                                                         ('Test 2', 'Test 3', 'Test 4'),
                                                         ('Test 2', 'Test 3')), (1, 4, 0.6))

    def test_get_root(self):
        root = Star(Node(('Test 3', 'Test 4'), 0.2),
                    clusters=[Node(('Test 2', 'Test 3', 'Test 4'), 4),
                              Node(('Test 1', 'Test 3', 'Test 4'), 3)])
        self.assertEqual(self.algo.get_root(), root)


class TestSequential(TestCase):
    def setUp(self):
        self.algo = SequentialCherryTreeAlgorithm(data, 3)
        information = {('Test 1', 'Test 2', 'Test 3'): 1, ('Test 1', 'Test 2', 'Test 4'): 2,
                       ('Test 1', 'Test 3', 'Test 4'): 3, ('Test 2', 'Test 3', 'Test 4'): 4,
                       ('Test 1', 'Test 2'): 0.5, ('Test 1', 'Test 3'): 0.5,
                       ('Test 1', 'Test 4'): 0.4, ('Test 2', 'Test 3'): 0.6,
                       ('Test 2', 'Test 4'): 0.7, ('Test 3', 'Test 4'): 0.2,
                       'Test 1': 0, 'Test 2': 0, 'Test 3': 0, 'Test 4': 0,
                       ('Test 1', 'Test 2', 'Test 3', 'Test 4'): 10}
        self.algo.input_object.information = information

    def test_check_base_tree_negativ(self):
        with self.assertRaises(ValueError):
            SequentialCherryTreeAlgorithm(data, 3, base_tree=JunctionTree())

    def test_check_base_tree_positiv(self):
        with_base = SequentialCherryTreeAlgorithm(data, 3, base_tree=CherryTree())
        self.assertEqual(type(with_base.base_tree), CherryTree)

    def test_check_width(self):
        with self.assertRaises(ValueError):
            SequentialCherryTreeAlgorithm(data, 2)
        cherry_3 = CherryTree()

        def get_width():
            return 3

        cherry_3.get_width = get_width
        with self.assertRaises(ValueError):
            SequentialCherryTreeAlgorithm(data, 3, base_tree=cherry_3)

    def test_get_big_star(self):
        small_star = Star(Node(('Test 1', ), 0), clusters=[Node(('Test 1', 'Test 2'), 0.5),
                                                           Node(('Test 1', 'Test 3'), 0.5)])
        self.assertEqual(self.algo.get_big_star(small_star, 3),
                         (small_star, [Star(clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1)])]))

    def test_build_cherry(self):
        cherry = CherryTree()
        star = Star(Node(('Test 1', 'Test 2'), 0.5),
                    clusters=[Node(('Test 1', 'Test 2', 'Test 3'), 1),
                              Node(('Test 1', 'Test 2', 'Test 4'), 2)])
        cherry.stars.append(star)
        cherry.variables = {'Test 1', 'Test 2', 'Test 3', 'Test 4'}
        cherry.clusters = [('Test 1', 'Test 2', 'Test 3'), ('Test 1', 'Test 2', 'Test 4')]
        cherry.weight = 2.34190119
        self.assertEqual(self.algo.build_cherry_tree(), cherry)


if __name__ == "__main__":
    main()
