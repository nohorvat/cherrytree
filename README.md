# Cherry Tree
This package contains three algorithms for constructing cherry trees. A cherry tree is a special junction tree which contains in the clusters *k* elements and in the separators *k-1* elements.

The algorithms are the following:
- *Chow-Liu Tree Algorithm*: We present a method to approximate optimally an *n*-dimensional discrete probability distribution by a product of second-order distributions or the distribution of the first-order tree dependence. This method is introduced in [1].
- *Cherry Tree Algorithm*: This is the original algorithm introduced in [2]. This is recommended for variables up to 50.
- *Improved Cherry Tree Algorithm*: This method gives in general the best approximation of the real distribution. This algorithm starts by selecting the best two clusters as root instead of selecting only the best one. This is recommended for variables up to 50.
- *Sequentially Improved Cherry Tree Algorithm*: This algorithm improves sequentionally a *k*-cherry tree into a *k+1* cherry tree. This is the fastest method which is suitable for large feature set. The algorithm can be started from the Chow-Liu tree which is improved sequentionally to a given *k*-th order cherry tree.
- *Frozen Cherry Tree Algorithm*: TODO

You can find a detailed documentation [here](https://nohorvat.gitlab.io/cherrytree/).

### Todos

 - Write MORE Tests
 - Sequentilly Improved Cherry Tree Algorithm
 - Frozen Cherry Tree Algorithm

License
----
MIT License

Copyright (c) 2019 Noemi Horvath

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

---
### References
[1] **C.K. Chow and C.N. Liu** *Approximating discrete probability distributions with dependence trees.* IEEE transactions on Information Theory, 14(3):462–467, 1968.

[2] **Tamás Szántai and Edith Kovács** *Hypergraphs as a mean of discovering the dependence structure of a discrete multivariate probability distribution.* Annals of Operations Research, 193(1):71–90, 2012.