Welcome to cherrytree's documentation!
============================================

Contents:

.. toctree::

   algo_docs/intro.rst
   algo_docs/utils.rst
   algo_docs/algorithms.rst
   algo_docs/example.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

