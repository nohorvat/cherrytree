Utils
=====

.. automodule:: cherrytree.utils

.. toctree::

   base.rst
   discretize.rst