Input handler
=============

.. autoclass:: cherrytree.utils.base.InputHandler
   :members:

Star
====

.. autoclass:: cherrytree.utils.base.Star
   :members:

Junction tree and Cherry tree
=============================

.. autoclass:: cherrytree.utils.base.JunctionTree
   :members:

.. autoclass:: cherrytree.utils.base.CherryTree
   :members:

Base
====

.. autoclass:: cherrytree.utils.base.Base
   :members: