Example
=======

Data set
--------
In this example we work with the *Breast Cancer Wisconsin (Diagnostic) Data Set*, which can be
download from `Kaggle <https://www.kaggle.com/uciml/breast-cancer-wisconsin-data/>`_.

We use *pandas* to read, process and work with the data.

First, we read the csv file, and view our data.

.. code-block:: python

    >>> import pandas as pd
    >>> df = pd.read_csv('data.csv')
    >>> df.head()
             id diagnosis     ...       fractal_dimension_worst  Unnamed: 32
    0    842302         M     ...                       0.11890          NaN
    1    842517         M     ...                       0.08902          NaN
    2  84300903         M     ...                       0.08758          NaN
    3  84348301         M     ...                       0.17300          NaN
    4  84358402         M     ...                       0.07678          NaN
    >>> df.drop(['id', 'Unnamed: 32'], axis=1, inplace=True)
    >>> df.head()
          diagnosis           ...             fractal_dimension_worst
    0         M           ...                             0.11890
    1         M           ...                             0.08902
    2         M           ...                             0.08758
    3         M           ...                             0.17300
    4         M           ...                             0.07678


We have to discretize the data. The *diagnosis* column is a string valued column, we transform it
into integers.

.. code-block:: python

    >>> from cherrytree.utils.discretize import discretize_quantile
    >>> discretize_quantile(df, list(df)[1:], 5)
    >>> df['diagnosis'].replace({'M': 0, 'B': 1}, inplace=True)
    >>> df.info()
    diagnosis                  569 non-null int64
    radius_mean                569 non-null int64
    texture_mean               569 non-null int64
    perimeter_mean             569 non-null int64
    area_mean                  569 non-null int64
    smoothness_mean            569 non-null int64
    compactness_mean           569 non-null int64
    concavity_mean             569 non-null int64
    concave points_mean        569 non-null int64
    symmetry_mean              569 non-null int64
    fractal_dimension_mean     569 non-null int64
    radius_se                  569 non-null int64
    texture_se                 569 non-null int64
    perimeter_se               569 non-null int64
    area_se                    569 non-null int64
    smoothness_se              569 non-null int64
    compactness_se             569 non-null int64
    concavity_se               569 non-null int64
    concave points_se          569 non-null int64
    symmetry_se                569 non-null int64
    fractal_dimension_se       569 non-null int64
    radius_worst               569 non-null int64
    texture_worst              569 non-null int64
    perimeter_worst            569 non-null int64
    area_worst                 569 non-null int64
    smoothness_worst           569 non-null int64
    compactness_worst          569 non-null int64
    concavity_worst            569 non-null int64
    concave points_worst       569 non-null int64
    symmetry_worst             569 non-null int64
    fractal_dimension_worst    569 non-null int64


Building the cherry tree
------------------------
After the data is preprocessed, we can build up the cherry tree. We use the Improved Cherry Tree algorithm (from scratch).

.. code-block:: python

    >>> from cherrytree.algorithms import ImprovedCherryTreeAlgorithm
    >>> algo = ImprovedCherryTreeAlgorithm(df, 3)
    >>> cherry = algo.build_cherry_tree()

Now we have a built cherry tree with weight, clusters and stars.

.. code-block:: python

    >>> cherry.clusters
    [('area_mean', 'radius_mean', 'radius_worst'), ('area_mean', 'area_worst', 'radius_worst'), ('perimeter_mean', 'radius_mean', 'radius_worst'), ('perimeter_mean', 'perimeter_worst', 'radius_worst'), ('concave points_mean', 'perimeter_worst', 'radius_worst'), ('concave points_mean', 'concave points_worst', 'perimeter_worst'), ('concave points_mean', 'concave points_worst', 'concavity_mean'), ('concave points_worst', 'concavity_mean', 'concavity_worst'), ('compactness_worst', 'concave points_worst', 'concavity_worst'), ('compactness_mean', 'compactness_worst', 'concave points_worst'), ('concave points_worst', 'concavity_mean', 'concavity_se'), ('compactness_se', 'concave points_worst', 'concavity_se'), ('concave points_se', 'concave points_worst', 'concavity_se'), ('area_se', 'concave points_mean', 'radius_worst'), ('area_se', 'radius_se', 'radius_worst'), ('area_se', 'perimeter_se', 'radius_se'), ('concave points_worst', 'diagnosis', 'perimeter_worst'), ('compactness_se', 'concave points_worst', 'fractal_dimension_se'), ('concave points_worst', 'fractal_dimension_se', 'fractal_dimension_worst'), ('concave points_worst', 'fractal_dimension_mean', 'fractal_dimension_worst'), ('concave points_worst', 'fractal_dimension_mean', 'smoothness_mean'), ('concave points_worst', 'smoothness_mean', 'smoothness_worst'), ('radius_se', 'radius_worst', 'smoothness_se'), ('concave points_worst', 'fractal_dimension_mean', 'symmetry_mean'), ('concave points_worst', 'symmetry_mean', 'symmetry_worst'), ('concave points_se', 'concave points_worst', 'symmetry_se'), ('radius_se', 'radius_worst', 'texture_se'), ('radius_worst', 'texture_se', 'texture_worst'), ('texture_mean', 'texture_se', 'texture_worst')]
    >>> cherry.stars
    [('area_mean', 'radius_worst')**[('area_mean', 'radius_mean', 'radius_worst'), ('area_mean', 'area_worst', 'radius_worst')], ('radius_mean', 'radius_worst')**[('perimeter_mean', 'radius_mean', 'radius_worst'), ('area_mean', 'radius_mean', 'radius_worst')], ('perimeter_mean', 'radius_worst')**[('perimeter_mean', 'perimeter_worst', 'radius_worst'), ('perimeter_mean', 'radius_mean', 'radius_worst')], ('perimeter_worst', 'radius_worst')**[('concave points_mean', 'perimeter_worst', 'radius_worst'), ('perimeter_mean', 'perimeter_worst', 'radius_worst')], ('concave points_mean', 'perimeter_worst')**[('concave points_mean', 'concave points_worst', 'perimeter_worst'), ('concave points_mean', 'perimeter_worst', 'radius_worst')], ('concave points_mean', 'concave points_worst')**[('concave points_mean', 'concave points_worst', 'concavity_mean'), ('concave points_mean', 'concave points_worst', 'perimeter_worst')], ('concave points_worst', 'concavity_mean')**[('concave points_worst', 'concavity_mean', 'concavity_worst'), ('concave points_mean', 'concave points_worst', 'concavity_mean'), ('concave points_worst', 'concavity_mean', 'concavity_se')], ('concave points_worst', 'concavity_worst')**[('compactness_worst', 'concave points_worst', 'concavity_worst'), ('concave points_worst', 'concavity_mean', 'concavity_worst')], ('compactness_worst', 'concave points_worst')**[('compactness_mean', 'compactness_worst', 'concave points_worst'), ('compactness_worst', 'concave points_worst', 'concavity_worst')], ('concave points_worst', 'concavity_se')**[('compactness_se', 'concave points_worst', 'concavity_se'), ('concave points_worst', 'concavity_mean', 'concavity_se'), ('concave points_se', 'concave points_worst', 'concavity_se')], ('concave points_mean', 'radius_worst')**[('area_se', 'concave points_mean', 'radius_worst'), ('concave points_mean', 'perimeter_worst', 'radius_worst')], ('area_se', 'radius_worst')**[('area_se', 'radius_se', 'radius_worst'), ('area_se', 'concave points_mean', 'radius_worst')], ('area_se', 'radius_se')**[('area_se', 'perimeter_se', 'radius_se'), ('area_se', 'radius_se', 'radius_worst')], ('concave points_worst', 'perimeter_worst')**[('concave points_worst', 'diagnosis', 'perimeter_worst'), ('concave points_mean', 'concave points_worst', 'perimeter_worst')], ('compactness_se', 'concave points_worst')**[('compactness_se', 'concave points_worst', 'fractal_dimension_se'), ('compactness_se', 'concave points_worst', 'concavity_se')], ('concave points_worst', 'fractal_dimension_se')**[('concave points_worst', 'fractal_dimension_se', 'fractal_dimension_worst'), ('compactness_se', 'concave points_worst', 'fractal_dimension_se')], ('concave points_worst', 'fractal_dimension_worst')**[('concave points_worst', 'fractal_dimension_mean', 'fractal_dimension_worst'), ('concave points_worst', 'fractal_dimension_se', 'fractal_dimension_worst')], ('concave points_worst', 'fractal_dimension_mean')**[('concave points_worst', 'fractal_dimension_mean', 'smoothness_mean'), ('concave points_worst', 'fractal_dimension_mean', 'fractal_dimension_worst'), ('concave points_worst', 'fractal_dimension_mean', 'symmetry_mean')], ('concave points_worst', 'smoothness_mean')**[('concave points_worst', 'smoothness_mean', 'smoothness_worst'), ('concave points_worst', 'fractal_dimension_mean', 'smoothness_mean')], ('radius_se', 'radius_worst')**[('radius_se', 'radius_worst', 'smoothness_se'), ('area_se', 'radius_se', 'radius_worst'), ('radius_se', 'radius_worst', 'texture_se')], ('concave points_worst', 'symmetry_mean')**[('concave points_worst', 'symmetry_mean', 'symmetry_worst'), ('concave points_worst', 'fractal_dimension_mean', 'symmetry_mean')], ('concave points_se', 'concave points_worst')**[('concave points_se', 'concave points_worst', 'symmetry_se'), ('concave points_se', 'concave points_worst', 'concavity_se')], ('radius_worst', 'texture_se')**[('radius_worst', 'texture_se', 'texture_worst'), ('radius_se', 'radius_worst', 'texture_se')], ('texture_se', 'texture_worst')**[('texture_mean', 'texture_se', 'texture_worst'), ('radius_worst', 'texture_se', 'texture_worst')]]
    >>> cherry.weight
    30.762204602324367

We can get the relevant variables to one given feature. The relevant variables are
those which share the same clusters with the given feature. The relevance means neighbors in
a Markov network with the local Markov property.

.. code-block:: python

    >>> algo.get_relevant_variables('diagnosis')
    {'perimeter_worst', 'concave points_worst'}


Building the cherry tree with constraints
-----------------------------------------
In case of the Cherry Tree algorithm and the Improved Cherry Tree algorithm, we can build the
cherry tree with constraints.

With the *include* parameter we can give the name of the variables which must occur in all clusters
(and hence separators).

.. code-block:: python

    >>> from cherrytree.algorithms import CherryTreeAlgorithm
    >>> algo = CherryTreeAlgorithm(df, 3, include=('diagnosis',))
    >>> cherry = algo.build_cherry_tree()
    >>> cherry.draw_cherry_tree()

With the *exclude* parameter we can give the name of the variables which must occur in only one
cluster (and hence none of the separators).

.. code-block:: python

    >>> algo = CherryTreeAlgorithm(df, 3, exclude=('diagnosis',))
    >>> cherry = algo.build_cherry_tree()
    >>> cherry.draw_cherry_tree()

With the *start_from* parameter we can give the name of the variables which must occur in the root
cluster(s).

.. code-block:: python

    >>> algo = CherryTreeAlgorithm(A, 3, start_from=('diagnosis', 'radius_worst'))
    >>> cherry = algo.build_cherry_tree()
    >>> cherry.draw_cherry_tree()
