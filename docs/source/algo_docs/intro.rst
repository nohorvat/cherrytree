First steps
===========

Installing
----------
You can install the latest estable version from PyPI.

.. code-block:: bash

   $ pip install cherrytree

Or if you want to work with the latest development version, you can install it from GitLab.

.. code-block:: bash

   $ pip install git+https://gitlab.com/nohorvat/cherry-tree.git

Requirements
------------
You can install the project requirements using *pip* package manager.

.. code-block:: bash

    $ pip install -r requirements.txt

Cleaning data
-------------
The program works with :py:class:`pandas.DataFrame` objects. The given dataframe must contains only
the random variables (features) which are needed, the column headers should be the names of the
variables.

The algorithm works with discrete random variables, so if the variables are not discrete you have
to discretize them. If the columns of the dataframe have values which are obtained from continuous
random variables two methods for the discretization are proposed.

The first method is a quantile-based discretization function using the pandas built-in
:py:meth:`pandas.qcut` function.

.. code-block:: python

    >>> import pandas as pd
    >>> from cherrytree.utils.discretize import discretize_quantile
    >>> dataframe = pd.DataFrame(data=[{'A': 5.12, 'B': 6.56, 'C': 7.98},
    ...                                {'A': -0.45, 'B': 4.2, 'C': 1.2},
    ...                                {'A': 3.1, 'B': -1.02, 'C': 11}])
    >>> discretize_quantile(dataframe, ['A', 'C'], 3)
    >>> dataframe
        A       B   C
    0   2    6.56   1
    1   0    4.20   0
    2   1   -1.02   2
    >>> discretize_quantile(dataframe, ['B'], [0, .25, .75, 1])
    >>> dataframe
        A    B   C
    0   2    2   1
    1   0    1   0
    2   1    0   2

The second method makes possible the customization of the bins. This method is recommendable when
appropriate choice of the bins is possible for example based on consulting with experts. This
method sets the indices of the bins to which each value in input array belongs using the numpy
built-in function :py:meth:`numpy.digitize`.

.. code-block:: python

    >>> import pandas as pd
    >>> from cherrytree.utils.discretize import discretize_digitize
    >>> dataframe = pd.DataFrame(data=[{'A': 5.12, 'B': 6.56, 'C': 7.98},
    ...                                {'A': -0.45, 'B': 4.2, 'C': 1.2},
    ...                                {'A': 3.1, 'B': -1.02, 'C': 11}])
    >>> discretize_digitize(dataframe, ['A', 'C'], [2.1, 5.1, 12.5])
    >>> dataframe
        A       B   C
    0   2    6.56   2
    1   0    4.20   0
    2   1   -1.02   2

For further information see the documentation for the module :mod:`~cherrytree.utils.discretize`.

Usage
-----

The fitting of a cherry tree probability distribution to the data and the building of the cherry tree
*****************************************************************************************************
This package contains three algorithms for constructing cherry trees. A cherry tree is a special
junction tree which contains in the clusters *k* elements and in the separators *k-1* elements.

The algorithms are the following:
    - *Chow-Liu Tree Algorithm*: This method is introduced in [Chow-Liu cikk].
    - *Cherry Tree Algorithm*: This is the original algorithm introduced in [Hipergráfos cikk].
      This is recommended for variables up to 50.
    - *Improved Cherry Tree Algorithm*: This method gives in general the best approximation of the real
      distribution. This algorithm starts by selecting the best two clusters as root instead of selecting
      only the best one. This is recommended for variables up to 50.


First one have to build the junction tree as follows.

.. code-block:: python

    >>> from cherrytree.algorithms import CherryTreeAlgorithm
    >>> import pandas as pd
    >>> dataframe = pd.DataFrame(data=[{'A': 2, 'B': 2, 'C': 1},
    ...                                {'A': 0, 'B': 1, 'C': 0},
    ...                                {'A': 1, 'B': 0, 'C': 2}])
    >>> cherry_algo = CherryTreeAlgorithm(dataframe, cluster_width=2)
    >>> junction_tree = cherry_algo.build_junction_tree()

This works in the same way for all of the algorithms.

The cherry tree
***************
The clusters, stars, the weight of the obtained cherry tree can be listed.

.. code-block:: python

    >>> junction_tree.clusters
    >>> junction_tree.stars
    >>> junction_tree.weight

We can get the set of relevant variables for a given target variable.

.. code-block:: python

    >>> cherry_algo.get_relevant_variables('A')

We can get the approximation of the probability of a given realization based on the cherry tree
probability distribution.

.. code-block:: python

    >>> cherry_algo.prob_realization({'A': 1, 'B': 2, 'C': 0})

We can get the information content of set of variables belonging to the clusters.

.. code-block:: python

    >>> cherry_algo.input_object.get_information_content('A', 'C')

For further information see the documentation for the module :mod:`~cherrytree.utils.base`.
