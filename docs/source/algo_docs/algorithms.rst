Algorithms
==========

.. automodule:: cherrytree.algorithms

.. toctree::

   chow_liu_tree.rst
   cherry.rst
   improved_cherry.rst
